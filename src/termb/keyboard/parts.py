#!/usr/bin/env python
from __future__ import annotations

from solid import *
from solid.utils import *

import math

from termb.configuration import Configuration

from termb.generic.forms import RoundingStamp
from termb.generic.promicro import PromicMount, PromicFrontCutaway, PromicBackBarCutaway
from termb.generic.nut import Nut
from termb.generic.flat_head_srew import ScrewFlatHead

import termb.keyboard.caps as caps
from termb.keyboard.kernel import MainKeyField, ThumbKeyField
from termb.keyboard.kernel import Guts, Promic


class ExtraNob:
    """
    The extra nob, used by both plate decorations to better fix the concave part of the decoration.

    This class just provides the footprints for the nob itself, the counter-hole in the switch plates,
    and the cover, visible from the top and connecting the nob to the decoration parts.

    x/y = 0/0 at y-center and x-bottom.
    """
    def __init__(self, configuration):
        self.c = configuration

    @property
    def cover_footprint(self):
        rounding = RoundingStamp(radius=self.c.decoration_nob_plate_corner_radius)
        return left(self.c.decoration_nob_plate_x_length/2)(
            square((self.c.decoration_nob_plate_x_length, self.c.decoration_nob_plate_y_length))
            -
            translate((self.c.decoration_nob_plate_x_length + 0.001, self.c.decoration_nob_plate_y_length + 0.001, 0))(rounding())
            -
            translate((0, self.c.decoration_nob_plate_y_length + 0.001, 0))(rotate((0, 0, 90))(rounding()))
        )

    @property
    def nob_footprint(self):
        return left(self.c.decoration_nob_plate_x_length/2)(
            translate((
                self.c.decoration_nob_corner_radius + (self.c.decoration_nob_plate_x_length - self.c.decoration_nob_x_length) / 2,
                self.c.decoration_nob_corner_radius + (self.c.decoration_nob_plate_y_length - self.c.decoration_nob_y_length) / 2 + self.c.decoration_top_nob_y_offset,
                0
            ))(
                minkowski()(
                    square((
                        self.c.decoration_nob_x_length - 2 * self.c.decoration_nob_corner_radius,
                        self.c.decoration_nob_y_length - 2 * self.c.decoration_nob_corner_radius
                    )),
                    circle(r=self.c.decoration_nob_corner_radius, segments=60)
                )
            )
        )

    @property
    def nob_hole_footprint(self):
        x = self.c.decoration_nob_x_length + 2*self.c.decoration_nob_horizontal_clearance
        y = self.c.decoration_nob_y_length + 2*self.c.decoration_nob_horizontal_clearance
        return left(self.c.decoration_nob_plate_x_length/2)(
            translate((
                (self.c.decoration_nob_plate_x_length - x) / 2,
                (self.c.decoration_nob_plate_y_length - y) / 2 + self.c.decoration_top_nob_y_offset,
                0
            ))(
                square((x, y)),
            )
        )


class Part:

    def __init__(self, configuration: Configuration):
        self.c = configuration
        self.main_key_field = MainKeyField(configuration)
        self.thumb_key_field = ThumbKeyField(configuration, self.main_key_field)
        self.promic = Promic(configuration, self.main_key_field)
        self.guts = Guts(configuration, self.main_key_field, self.thumb_key_field, self.promic)
        self.side_angle = math.degrees(math.atan(
            ((self.c.back_plate_screw_block_base_width - self.c.back_plate_screw_block_width) / 2)
            /
            self.c.back_plate_spacing
        ))

    def back_plate_screw_transition(self, o):
        h = self.c.back_plate_spacing
        return down(h + self.c.switch_plate_thickness)(
            up(
                self.c.back_plate_spacing
                -
                self.c.back_plate_screw_lift
                -
                self.c.back_plate_screw_head_clearance
            )(
                o
            )
        )

    @property
    def raw_back_plate_screw(self):
        return ScrewFlatHead(
            d=self.c.screw_hole_diameter + self.c.back_plate_screw_diameter_clearance,
            dh=self.c.screw_head_hole_diameter + self.c.back_plate_screw_head_diameter_clearance,
            ah=90,
            l=self.c.screw_length_back_plate,
            head_elongation=self.c.back_plate_screw_head_clearance
        )

    @property
    def back_plate_screw_block(self):
        h = self.c.back_plate_spacing
        screw = self.raw_back_plate_screw
        screw_slot = rotate((0, -90, 0))(
            linear_extrude(100)(projection()(
                rotate((0, 90, 0))(
                    screw()
                )
            ))
        )
        return up(0)(
            down(h + self.c.switch_plate_thickness)(
                right(self.c.back_plate_screw_block_offset)(up(h / 2)(
                    cube((
                        self.c.back_plate_screw_block_thickness,
                        self.c.back_plate_screw_block_base_width,
                        h
                    ), center=True)
                ))
                -
                left(25)(
                    forward(self.c.back_plate_screw_block_width / 2)(
                        rotate((-self.side_angle, 0, 0))(
                            cube((50, 50, 50))
                        )
                    )
                    +
                    back(self.c.back_plate_screw_block_width / 2)(
                        rotate((self.side_angle, 0, 0))(
                            back(50)(cube((50, 50, 50)))
                        )
                    )
                )
            )
            -
            self.back_plate_screw_transition(
                screw()
                +
                screw_slot
            )
        )

    @staticmethod
    def _factor_tilt_block(height, tilt, x_tilt):
        return up(height)(
            rotate((x_tilt, 0, 0))(
                rotate((0, -tilt, 0))(
                    translate((-10, -100, 0))(cube([200, 300, 100]))
                )
            )
        )

    @property
    def upper_main_cutaway(self):
        return self._factor_tilt_block(self.c.outer_height, self.c.tilt, self.c.x_tilt)

    @property
    def decoration_base_cutaway(self):
        """
        Upper cutaway of the base decoration part
        """
        return self._factor_tilt_block(
            self.c.decoration_base_outer_height,
            self.c.decoration_base_tilt,
            self.c.decoration_base_x_tilt
        )

    @property
    def decoration_finger_cutaway(self):
        """
        *Lower* cutaway of the finger decoration part
        """
        return up(0)(
            translate((-10, -100, -1))(cube((200, 400, 200)))
            -
            self._factor_tilt_block(
                self.c.decoration_finger_outer_height,
                self.c.decoration_finger_tilt,
                self.c.decoration_finger_x_tilt
            )
        )

    @property
    def upper_thumb_cutaway(self):
        return translate(self.thumb_key_field.xy_offset)(  # (back(80 - self.c.corner_radius)(left(10)(
            translate((-20, -200 + 20, 0))(
                up(self.c.thumb_cluster_height)(
                    cube([200, 200, 100])
                )
            )
        )

    @property
    def footprint(self):
        return self.main_key_field.footprint + self.thumb_key_field.footprint

    @property
    def thumb_footprint(self):
        return self.thumb_key_field.footprint - self.main_key_field.footprint

    @property
    def thumb_cutaway_footprint(self):
        return offset(r=-self.c.wall_width, segments=40)(self.thumb_footprint)

    @property
    def thumb_cutaway_footprint_with_clearance(self):
        return offset(r=-self.c.wall_width - self.c.wall_clearance, segments=40)(self.thumb_footprint)

    @property
    def finger_footprint(self):
        return self.main_key_field.footprint

    @property
    def finger_cutaway_footprint(self):
        return offset(r=-self.c.wall_width, segments=40)(self.finger_footprint)

    @property
    def finger_cutaway_footprint_with_clearance(self):
        return offset(r=-self.c.wall_width - self.c.wall_clearance, segments=40)(self.finger_footprint)

    @property
    def finger_wall_footprint_with_clearances(self):
        return offset(
            r=self.c.wall_outside_clearance,
            segments=40
        )(self.finger_footprint) - self.finger_cutaway_footprint_with_clearance

    @property
    def base_deco_footprint(self):
        return offset(r=self.c.decoration_wall_thickness + self.c.decoration_wall_clearance, segments=40)(self.footprint)

    @property
    def base_deco_footprint_with_outer_clearance(self):
        """
        Enlarged base deco footprint for the cutout on the USB plate.
        :return:
        """
        return offset(
            r=self.c.decoration_wall_thickness + self.c.decoration_wall_clearance + self.c.decoration_base_usb_plate_front_clearance,
            segments=40
        )(
            self.footprint
        )

    @property
    def finger_deco_footprint(self):
        return offset(r=self.c.decoration_wall_thickness + self.c.decoration_wall_clearance, segments=40)(self.finger_footprint)

    @property
    def thumb_deco_footprint(self):
        return up(0)(
            offset(r=self.c.decoration_wall_thickness + self.c.decoration_wall_clearance, segments=40)(self.thumb_footprint)
            -
            self.finger_footprint
        )

    @property
    def base_deco_clearance_footprint(self):
        return offset(r=self.c.decoration_wall_clearance, segments=40)(self.footprint)

    @property
    def finger_deco_clearance_footprint(self):
        return offset(r=self.c.decoration_wall_clearance, segments=40)(self.finger_footprint)

    @property
    def thumb_deco_clearance_footprint(self):
        return up(0)(
            offset(r=self.c.decoration_wall_clearance, segments=40)(self.thumb_footprint)
            -
            self.finger_footprint
        )

    @property
    def base_deco_cutaway_footprint(self):
        return offset(r=-self.c.decoration_base_rim, segments=40)(self.footprint)

    @property
    def finger_deco_cutaway_footprint(self):
        return offset(r=-self.c.decoration_finger_rim, segments=40)(self.finger_footprint)

    @property
    def thumb_deco_cutaway_footprint(self):
        return offset(r=-self.c.decoration_thumb_rim, segments=40)(self.thumb_footprint)

    def _pole(self, thickness):
        full_square_w = 16
        square_w = full_square_w - 2 * self.c.screw_pole_rounding_radius
        corner_r = self.c.screw_pole_rounding_radius
        _offset = 8 - (self.c.screw_hole_diameter / 2 + thickness)
        return up(0)(
            translate((-_offset, -_offset, 0))(
                translate((-full_square_w / 2, -full_square_w / 2, 0))(
                    translate((corner_r, corner_r, 0))(
                        minkowski()(
                            square(square_w),
                            circle(r=corner_r, segments=30)
                        )
                    )
                )
            )
        )

    def poles(self, screw_positions, base_heights, intersect, skip_screws=False):
        s = self.c.screw_pole_smoothing_length
        result = sum(
            [
                linear_extrude(100)(
                    offset(r=s, segments=50)(offset(r=-s, segments=50)(
                        intersection()(
                            translate(p.c)(
                                rotate(p.angle)(
                                    self._pole(self.c.screw_pole_thickness)
                                )
                            ),
                            intersect
                        )
                    ))
                )
                +
                linear_extrude(bh)(
                    offset(r=s, segments=50)(offset(r=-s, segments=50)(
                        intersection()(
                            translate(p.c)(
                                rotate(p.angle)(
                                    self._pole(self.c.screw_pole_base_thickness)
                                )
                            ),
                            intersect
                        )
                    ))
                )
                -
                (
                    color("red")(
                        down(1)(linear_extrude(102)(
                            translate(p.c)(
                                circle(d=self.c.screw_hole_diameter, segments=50))
                            )
                        )
                    )
                    if not skip_screws
                    else 0
                )
                for p, bh in zip(screw_positions, base_heights)
            ]
        )
        return result

    def finger_screw_poles(self, skip_screws=False):
        return self.poles(
            self.main_key_field.screw_positions,
            [h + self.c.screw_pole_base_height_above_nut for h in self.c.nut_depths_main_part],
            self.finger_cutaway_footprint_with_clearance,
            skip_screws
        )

    def thumb_screw_poles(self, skip_screws=False):
        return self.poles(
            self.thumb_key_field.screw_positions,
            len(self.thumb_key_field.screw_positions)
            *
            [self.c.nut_depth_thumb_cluster + self.c.screw_pole_base_height_above_nut],
            self.thumb_cutaway_footprint_with_clearance,
            skip_screws
        )

    @property
    def _connector_bracing(self):
        return up(self.c.base_plate_thickness + self.c.bottom_connector_height - 0.01)(
            rotate((90, 0, 0))(
                left(self.c.bottom_connector_diameter / 2)(
                    cube((self.c.bottom_connector_diameter, self.c.bottom_connector_diameter, self.c.bottom_connector_bracing_thickness))
                    -
                    right(self.c.bottom_connector_diameter)(
                        rotate((0, 0, 45))(
                            down(1)(
                                cube((
                                    self.c.bottom_connector_diameter * 2,
                                    self.c.bottom_connector_diameter * 2,
                                    self.c.bottom_connector_bracing_thickness + 2
                                ))
                            )
                        )
                    )
                )
            )
        )

    @property
    def bottom_screw_connectors(self):
        connector = up(self.c.base_plate_thickness)(
            rotate((0, 0, 180))(
                back(self.c.bottom_connector_diameter/2)(
                    cube((self.c.bottom_connector_diameter, self.c.bottom_connector_diameter, self.c.bottom_connector_height))
                )
                +
                cylinder(d=self.c.bottom_connector_diameter, h=self.c.bottom_connector_height, segments=40)
            )
        )
        bracing = left(0.5 * self.c.bottom_connector_diameter + 0.1)(
            self._connector_bracing
        )
        return intersection()(
            sum(
                [
                    translate((p.x, p.y, 0))(
                        rotate(p.angle)(
                            connector
                            +
                            back(self.c.bottom_connector_diameter/2 - self.c.bottom_connector_bracing_thickness)(bracing)
                            +
                            forward(self.c.bottom_connector_diameter/2)(bracing)
                        )
                    )
                    for p in self.guts.extra_bottom_screw_positions
                ]
            ),
            linear_extrude(100)(self.footprint)
        )



class ThumbPlate(Part):

    @property
    def notch_cutaway(self):
        s = self.c.screw_pole_smoothing_length
        return up(0)(
            linear_extrude(
                self.c.thumb_cluster_height
                -
                self.c.switch_plate_thickness
                +
                self.c.switch_plate_notch_height
            )(
                self.thumb_footprint
                -
                offset(r=s, segments=50)(offset(r=-s, segments=50)(
                    self.thumb_cutaway_footprint_with_clearance
                ))
            )
        )

    @property
    def counter_poles(self):
        return up(0)(
            intersection()(
                self.thumb_screw_poles(),
                down(
                    self.c.switch_plate_thickness + self.c.screw_pole_counter_length)(
                )(
                    self.upper_thumb_cutaway
                )
            )
            -
            down(self.c.switch_plate_thickness)(self.upper_thumb_cutaway)
        )

    def __call__(self):
        nob = ExtraNob(self.c)
        return up(0)(
            up(self.c.thumb_cluster_height - self.c.switch_plate_thickness)(
                linear_extrude(self.c.switch_plate_thickness)(self.thumb_footprint)
            )
            +
            self.counter_poles
            -
            self.notch_cutaway
            -
            self.thumb_key_field.as_stamps
            -
            self.screws
            -
            self.thumb_key_field.nob_translate(linear_extrude(100)(nob.nob_hole_footprint))
            +
            self.thumb_key_field.for_all_back_plate_screw_positions(self.back_plate_screw_block)
        )

    @property
    def screws(self):
        screw = ScrewFlatHead(
            d=self.c.screw_hole_diameter,
            dh=self.c.screw_head_hole_diameter,
            ah=90,
            l=60,
            head_elongation=50,
            segments=60
        )
        return sum(
            [
                translate(t)(
                    screw()
                ) for t in self.thumb_key_field.screw_translations
            ]
        )


class FingerPlate(Part):

    @property
    def screws(self):
        screw = ScrewFlatHead(
            d=self.c.screw_hole_diameter,
            dh=self.c.screw_head_hole_diameter,
            ah=90,
            l=60,
            head_elongation=50,
            segments=60
        )
        return sum(
            [
                translate(t)(screw())
                for t in self.main_key_field.screw_translations
            ]
        )

    @property
    def notch_cutaway(self):
        return up(0)(
            linear_extrude(100)(
                self.finger_footprint - self.finger_cutaway_footprint_with_clearance
            )
            -
            down(self.c.finger_switch_plate_vertical_thickness - self.c.switch_plate_notch_height)(self.upper_main_cutaway)
        )

    @property
    def counter_poles(self):
        return up(0)(
            intersection()(
                self.finger_screw_poles(),
                down(
                    self.c.finger_switch_plate_vertical_thickness + self.c.screw_pole_counter_length)(
                )(
                    self.upper_main_cutaway
                )
            )
            -
            down(self.c.finger_switch_plate_vertical_thickness)(self.upper_main_cutaway)
        )

    def __call__(self):
        nob = ExtraNob(self.c)
        result = up(0)(
            intersection()(
                linear_extrude(100)(self.finger_footprint)
                -
                self.upper_main_cutaway,
                down(self.c.finger_switch_plate_vertical_thickness)(self.upper_main_cutaway)
            )
            +
            self.counter_poles
            -
            self.notch_cutaway
            -
            self.screws
            -
            self.main_key_field.as_stamps
            -
            self.main_key_field.nob_translate(linear_extrude(100)(nob.nob_hole_footprint))
            +
            self.main_key_field.for_all_back_plate_screw_positions(self.back_plate_screw_block)
        )
        if self.c.upper_encoder_enabled:
            result += self.main_key_field.upper_ecoder_socket_block
            result -= self.main_key_field.upper_encoder
        return result


class Middle(Part):

    @property
    def jack_housing(self):
        exit_point = self.guts.exit_point
        base_height = exit_point[2] - self.c.base_plate_thickness
        return intersection()(
            up(self.c.base_plate_thickness)(
                translate((exit_point[0], exit_point[1], 0))(
                    translate((-self.guts.jack_housing_x_length, -self.guts.jack_housing_y_length / 2, 0))(
                        cube((self.guts.jack_housing_x_length, self.guts.jack_housing_y_length, base_height))
                        +
                        up(base_height)(
                            forward(self.guts.jack_housing_y_length / 2)(
                                rotate((0, 90, 0))(
                                    cylinder(d=self.guts.jack_housing_y_length, h=self.guts.jack_housing_x_length, segments=60)
                                )
                            )
                        )
                        -
                        down(base_height)(
                            cube((self.guts.jack_housing_x_length + 2, self.guts.jack_housing_y_length, base_height))
                        )
                    )
                    -
                    translate((
                        -self.guts.jack_housing_x_length - 1,
                        -self.guts.jack_housing_y_length / 2 - 1,
                        base_height - self.c.jack_excavation_lowering
                    ))(
                        cube((
                            self.c.jack_excavation_length + 1,
                            self.guts.jack_housing_y_length + 2,
                            self.guts.jack_housing_y_length
                        ))
                    )
                    +
                    translate((-self.guts.jack_housing_x_length, +self.guts.jack_housing_y_length / 2 - 0.01, 0))(
                        cube((self.guts.jack_housing_x_length, self.guts.jack_housing_x_length, self.c.bottom_connector_height))
                        +
                        right(self.guts.jack_housing_x_length)(
                            forward(self.guts.jack_housing_x_length - self.c.bottom_connector_bracing_thickness)(
                                down(self.c.bottom_connector_height)(rotate((0, 0, 180))(self._connector_bracing))
                            )
                        )
                    )
                )
            ),
            linear_extrude(100)(self.finger_cutaway_footprint)
        )

    @property
    def cutout_dice(self):
        """
        Cutout dice for the hole for the cables towards the thumb cluster.
        """
        return up(self.c.base_plate_thickness)(
            right(self.main_key_field.thumb_x_indent + self.c.wall_width + self.c.cable_cutout_x_offset)(
                forward(self.main_key_field.thumb_y_indent - self.c.wall_width - 1)(
                    color("red")(cube((self.c.cable_cutout_width, 2*self.c.wall_width + 2, self.c.cable_cutout_height)))
                )
            )
        )

    def __call__(self):
        return up(0)(
            up(self.c.base_plate_thickness)(
                linear_extrude(100)(
                    self.finger_footprint - self.finger_cutaway_footprint
                )
            )
            +
            up(self.c.base_plate_thickness)(
                linear_extrude(
                    self.c.thumb_cluster_height
                    - self.c.base_plate_thickness
                    - self.c.switch_plate_thickness
                    + self.c.switch_plate_notch_height
                )(
                    self.thumb_footprint - self.thumb_cutaway_footprint
                )
            )
            +
            self.jack_housing
            +
            self.bottom_screw_connectors
            -
            self.guts.jack
            -
            self.guts.bottom_screws
            -
            down(self.c.finger_switch_plate_vertical_thickness - self.c.switch_plate_notch_height)(self.upper_main_cutaway)
            -
            down(self.c.finger_switch_plate_vertical_thickness + self.c.screw_pole_top_clearance)(
                # cutting away the indented part of the finger plate is only relevant for the outer bottom connector
                intersection()(
                    self.upper_main_cutaway,
                    linear_extrude(100)(self.finger_cutaway_footprint)
                )
            )
            -
            up(self.c.thumb_cluster_height - self.c.switch_plate_thickness + self.c.switch_plate_notch_height)(
                linear_extrude(100)(self.thumb_footprint)
            )
            -
            self.promic.usb_front_plate_notch_marker_with_clearance
            -
            self.cutout_dice
        )

    @property
    def test_jack_housing(self):
        """
        The section of the middle part with the jack housing.

        Just for the test print.
        """
        exit_point = self.guts.exit_point
        t = (
            exit_point[0] - self.guts.jack_housing_x_length - 1,
            exit_point[1] - self.guts.jack_housing_y_length / 2 - 1,
            self.c.base_plate_thickness
        )
        d = (
            self.guts.jack_housing_x_length + self.c.wall_width + 2,
            self.guts.jack_housing_y_length + self.guts.jack_housing_x_length + 2,
            exit_point[2] + self.guts.jack_housing_y_length / 2
        )
        return intersection()(
            self(),
            translate(t)(cube(d))
        )

    @property
    def unwalled(self):
        return up(0)(
            self.jack_housing
            +
            self.guts.jack
            +
            self.promic.cutaway
            +
            self.bottom_screw_connectors
            +
            color("red")(self.guts.bottom_screws)
        )


class BasePlate(Part):
    def __call__(self):
        lower_finger_screw_poles = up(0)(
            self.finger_screw_poles()
            -
            down(self.c.finger_switch_plate_vertical_thickness + self.c.screw_pole_top_clearance + self.c.screw_pole_counter_length)(
                self.upper_main_cutaway
            )
        )
        lower_thumb_screw_poles = up(0)(
            self.thumb_screw_poles()
            -
            down(self.c.switch_plate_thickness + self.c.screw_pole_top_clearance + self.c.screw_pole_counter_length)(
                self.upper_thumb_cutaway
            )
        )
        thumb_nut = Nut(self.c.nut_width + self.c.bottom_nut_extra_clearance, self.c.nut_depth_thumb_cluster + 0.1)
        return up(0)(
            linear_extrude(self.c.base_plate_thickness)(self.footprint)
            +
            lower_finger_screw_poles
            +
            lower_thumb_screw_poles
            -
            down(0.1)(sum([
                translate(p.c)(Nut(self.c.nut_width + self.c.bottom_nut_extra_clearance, h + 0.1)())
                for p, h in zip(self.main_key_field.screw_positions, self.c.nut_depths_main_part)
            ]))
            -
            down(0.1)(sum([translate(p.c)(thumb_nut()) for p in self.thumb_key_field.screw_positions]))
            -
            self.promic.back_screw
            -
            self.promic.side_screw
            -
            self.guts.bottom_screws
        )


class PromicMountPlate(Part):
    """
    USB plate notches: the left-right-up clearances are taken away from the wall (middle part), the front and back
    clearances are taken away from the USB plate.
    """

    @property
    def side_screw_connector(self):
        x, y = self.promic.side_screw_position
        _offset = y - self.c.promic_config.promic_width / 2
        return down(self.c.promic_plate_thickness)(
            linear_extrude(self.c.screw_length_promic_side - self.c.base_plate_thickness)(
                right(x)(
                    back(y)(
                        circle(d=self.c.bottom_connector_diameter, segments=40)
                        +
                        forward(_offset / 2)(
                            square((self.c.bottom_connector_diameter, _offset), center=True)
                        )
                    )
                )
            )
        )

    @property
    def usb_plate_notch_cutaway(self):
        return up(0)(
            linear_extrude(100)(self.finger_wall_footprint_with_clearances)
            -
            self.promic.usb_front_plate_notch_marker_without_clearance
        )

    def __call__(self):
        w = self.promic.usb_front_plate_width
        rounding = RoundingStamp(radius=self.c.usb_connector_corner_rounding_radius)
        return up(0)(
            self.promic.transition(
                PromicMount(self.c.promic_config)()
                +
                forward(-w / 2)(
                    down(self.c.promic_plate_thickness)(
                        cube((
                            self.c.promic_config.promic_length + self.c.promic_back_bar_width - self.c.promic_config.front_notch_depth,
                            w,
                            self.c.promic_plate_thickness
                        ))
                        +
                        left(self.c.promic_usb_plate_thickness)(
                            cube((self.c.promic_usb_plate_thickness, w, self.c.promic_usb_plate_height))
                        )
                        -
                        down(1)(
                            left(0.001)(
                                linear_extrude(50)(
                                    left(self.c.promic_usb_plate_thickness)(
                                        back(0.001)(
                                            rotate((0, 0, 180))(rounding())
                                        )
                                        +
                                        forward(w + 0.001)(
                                            rotate((0, 0, 90))(rounding())
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
                +
                self.side_screw_connector
                -
                PromicFrontCutaway(self.c.promic_config)()
            )
            -
            self.promic.back_screw
            -
            self.promic.side_screw
            -
            self.usb_plate_notch_cutaway
            -
            (
                linear_extrude(100)(self.base_deco_footprint_with_outer_clearance - self.finger_cutaway_footprint)
                -
                up(self.c.decoration_base_usb_plate_top_clearance)(self.decoration_base_cutaway)
                -
                self.promic.usb_front_plate_deco_notch_marker_without_clearance
            )
        )

    @property
    def promic_block(self):
        """
        Just a proxy block, visualiting the promic for test views.
        """
        return up(self.c.promic_config.notch_bottom_level)(
            self.promic.transition(
                back(self.c.promic_config.promic_width / 2)(
                    cube((self.c.promic_config.promic_length, self.c.promic_config.promic_width, self.c.promic_height))
                )
            )
        )


class PromicBackBar(Part):
    def __call__(self):
        w = self.c.promic_config.promic_width + 2 * self.c.promic_config.notch_bar_width
        return up(0)(
            self.promic.transition(
                right(
                    self.c.promic_config.promic_length
                    -
                    self.c.promic_config.front_notch_depth
                    -
                    self.c.promic_config.back_notch_depth
                    +
                    self.c.promic_back_bar_clearance
                )(
                    back(w / 2)(
                        color("blue")(cube([self.c.promic_back_bar_width, w, self.c.promic_back_bar_height]))
                    )
                )
                -
                PromicBackBarCutaway(self.c.promic_config)()
            )
            -
            self.promic.back_screw
        )


class BaseDeco(Part):
    def __call__(self):
        return up(0)(
            down(self.c.decoration_bottom_thickness)(
                linear_extrude(100)(self.base_deco_footprint)
            )
            -
            linear_extrude(100)(self.base_deco_clearance_footprint)
            -
            down(self.c.decoration_bottom_thickness + 1)(
                linear_extrude(110)(
                    self.base_deco_cutaway_footprint
                )
            )
            -
            self.decoration_base_cutaway
            -
            self.promic.usb_front_plate_deco_notch_marker_with_clearance
            -
            self.guts.jack
        )


class TopDeco:
    def __init__(self, configuration: Configuration):
        self.c = configuration

    @property
    def screw_cover_footprint(self):
        return rotate((0, 0, 180))(
            minkowski()(
                square((10, 10)),
                circle(r=self.c.screw_cover_radius, segments=50)
            )
        )

    def factor_screw_nobs(self, screw_positions):
        screw = up(
            self.c.decoration_top_vertical_screw_clearance
            +
            self.c.decoration_top_screw_nob_conical_height
        )(
            ScrewFlatHead(
                d=self.c.screw_hole_diameter,
                dh=self.c.screw_head_hole_diameter - 2*self.c.decoration_top_horizontal_screw_clearance,
                ah=90,
                l=10,
                head_elongation=50,
                segments=60
            )()
        )
        sub_cyl = down(50)(cylinder(
            d=self.c.screw_head_hole_diameter + 2*self.c.decoration_top_horizontal_screw_clearance + 0.1,
            h=self.c.decoration_top_vertical_screw_clearance + 50
        ))
        return sum([
            translate(t)(
                screw - sub_cyl
            ) for t in screw_positions
        ])


class FingerDeco(Part, TopDeco):

    @property
    def screw_nobs(self):
        return self.factor_screw_nobs(self.main_key_field.screw_translations)

    @property
    def screw_cover_cylinders(self):
        bolt = down(5)(
            linear_extrude(15)(self.screw_cover_footprint)
        )
        return sum([
            translate((
                p.x, p.y, self.main_key_field.z_for_base_coordinate((p.x, p.y))
            ))(
                rotate((self.c.x_tilt, self.c.tilt, p.angle))(bolt)
            ) for p in self.main_key_field.screw_positions
        ])

    @property
    def extra_nob(self):
        nob = ExtraNob(self.c)
        return intersection()(
            linear_extrude(100)(self.main_key_field.nob_translate(nob.nob_footprint)),
            down(self.c.decoration_top_nob_excess + self.c.finger_switch_plate_vertical_thickness)(self.upper_main_cutaway)
        )

    def __call__(self):
        nob = ExtraNob(self.c)
        return up(0)(
            # outer wall / main body
            linear_extrude(100)(self.finger_deco_footprint)
            -
            # remove inner cutaway
            linear_extrude(100)(self.finger_deco_cutaway_footprint)
            +
            # add screw covers
            intersection()(
                self.screw_cover_cylinders,
                linear_extrude(100)(self.finger_footprint)
            )
            +
            linear_extrude(100)(
                intersection()(
                    self.main_key_field.nob_translate(nob.cover_footprint)
                    +
                    # square to connect the extra nob to the *rounded* deco
                    self.main_key_field.nob_translate(
                        left(self.c.decoration_nob_plate_x_length/2)(back(150 - self.c.decoration_finger_rim)(
                            square([150, 150])
                        ))
                    ),
                    self.finger_footprint
                )
            )
            -
            # cut away the inner part (where the keyboard fits in)
            (
                    linear_extrude(100)(self.finger_deco_clearance_footprint)
                    -
                    self.upper_main_cutaway
            )
            +
            self.screw_nobs
            +
            self.extra_nob
            -
            # cut away the upper part
            up(self.c.decoration_finger_vertical_thickness)(self.upper_main_cutaway)
            -
            # cut away clearance towards thumb section
            left(self.c.decoration_finger_to_thumb_horizontal_cleareance)(
                linear_extrude(
                    self.c.thumb_cluster_height
                    +
                    self.c.decoration_top_thickness
                    +
                    self.c.decoration_finger_to_thumb_vertical_cleareance
                )(
                    self.thumb_deco_footprint
                )
            )
            -
            # cut away the lower part
            self.decoration_finger_cutaway
        )


class ThumbDeco(Part, TopDeco):
    @property
    def screw_nobs(self):
        return self.factor_screw_nobs(self.thumb_key_field.screw_translations)

    @staticmethod
    def get_cut_cube(from_z, to_z):
        return up(from_z)(
            back(150)(cube((300, 300, to_z - from_z)))
        )

    @property
    def screw_covers(self):
        return intersection()(
            sum([
                translate((p.x, p.y, 0))(
                   rotate((0, 0, p.angle))(
                       self.screw_cover_footprint
                   )
                ) for p in self.thumb_key_field.screw_positions
            ]),
            self.thumb_deco_footprint
        )

    @property
    def extra_nob(self):
        nob = ExtraNob(self.c)
        return intersection()(
            linear_extrude(100)(self.thumb_key_field.nob_translate(nob.nob_footprint)),
            down(self.c.decoration_top_nob_excess + self.c.switch_plate_thickness)(self.upper_thumb_cutaway)
        )

    def __call__(self):
        nob = ExtraNob(self.c)
        return up(0)(
            linear_extrude(100)(self.thumb_deco_footprint)
            -
            linear_extrude(100)(self.thumb_deco_cutaway_footprint)
            +
            linear_extrude(100)(self.screw_covers)
            +
            linear_extrude(100)(self.thumb_key_field.nob_translate(nob.cover_footprint))
            -
            linear_extrude(self.c.thumb_cluster_height)(self.thumb_deco_clearance_footprint)
            +
            self.extra_nob
            -
            self.get_cut_cube(-1, self.c.decoration_thumb_height)
            +
            self.screw_nobs
            -
            self.get_cut_cube(self.c.thumb_cluster_height + self.c.decoration_top_thickness, 150)
        )


class FingerBackPlate(Part):
    def __call__(self):
        shape = projection(cut=False)(
            rotate((0, self.c.tilt, 0))(
                rotate((-self.c.x_tilt, 0, 0))(
                    self.template_plate()
                )
            )
        )
        shape = offset(r=self.c.back_plate_rounding, segments=30)(
            offset(r=-self.c.back_plate_rounding)(
                offset(r=-self.c.back_plate_reduction, segments=30)(shape)
            )
        )
        plate = linear_extrude(self.c.back_plate_thickness)(shape)
        return up(0)(
            self.plate_transition(plate)
            -
            self.screws
        )

    @property
    def screws(self):
        return self.main_key_field.for_all_back_plate_screw_positions(
            self.back_plate_screw_transition(
                self.raw_back_plate_screw()
            )
        )

    def plate_transition(self, o):
        return up(
            self.c.outer_height
            -
            self.c.finger_switch_plate_vertical_thickness
            -
            self.c.finger_back_plate_vertical_spacing
        )(
            rotate((self.c.x_tilt, 0, 0))(
                rotate((0, -self.c.tilt, 0))(
                    o
                )
            )
        )

    def template_plate(self):
        plate = self.plate_transition(cube((200, 200, 0.4)))
        cutouts = up(0)(
            self.finger_screw_poles(skip_screws=True)
            +
            self.bottom_screw_connectors
            +
            right(self.promic.center_x)(
                forward(
                    self.main_key_field.y_width
                    -
                    self.c.wall_width
                    -
                    self.c.wall_clearance
                    -
                    self.c.back_plate_reduction
                )(
                    cube((
                        self.c.finger_back_plate_back_cable_cutout_pre_width,
                        2 * self.c.finger_back_plate_back_cable_cutout_pre_depth,
                        200
                    ), center=True)
                )
            )
            +
            right(self.promic.center_x)(
                forward(
                    self.main_key_field.thumb_y_indent
                    +
                    self.c.wall_width
                    +
                    self.c.wall_clearance
                    +
                    self.c.back_plate_reduction
                )(
                    cube((
                        self.c.finger_back_plate_front_cable_cutout_pre_width,
                        2 * self.c.finger_back_plate_front_cable_cutout_pre_depth,
                        200
                    ), center=True)
                )
            )
        )
        if self.c.upper_encoder_enabled:
            cutouts += self.main_key_field.upper_encoder
        return intersection()(
            plate
            -
            up(self.c.back_plate_minimum_bottom_spacing + self.c.base_plate_thickness)(
                down(200)(cube((200, 200, 200)))
            )
            -
            cutouts,
            linear_extrude(100)(self.finger_cutaway_footprint_with_clearance)
        )


class ThumbBackPlate(Part):
    def __call__(self):
        shape = projection(cut=False)(
            self.template_plate()
        )
        shape = offset(r=self.c.back_plate_rounding, segments=30)(
            offset(r=-self.c.back_plate_rounding)(
                offset(r=-self.c.back_plate_reduction, segments=30)(shape)
            )
        )
        plate = linear_extrude(self.c.back_plate_thickness)(shape)
        return up(0)(
            self.plate_transition(plate)
            -
            self.screws
        )

    @property
    def screws(self):
        return self.thumb_key_field.for_all_back_plate_screw_positions(
            self.back_plate_screw_transition(
                self.raw_back_plate_screw()
            )
        )

    def plate_transition(self, o):
        return up(
            self.c.thumb_cluster_height
            -
            self.c.switch_plate_thickness
            -
            self.c.back_plate_spacing
            -
            self.c.back_plate_thickness
        )(
            o
        )

    def template_plate(self):
        cable_cutout_center_x = 1*(
            self.main_key_field.thumb_x_indent
            + self.c.wall_width
            + self.c.cable_cutout_x_offset
            + self.c.cable_cutout_width / 2
        )
        return up(0)(
            self.plate_transition(linear_extrude(0.1)(self.thumb_cutaway_footprint_with_clearance))
            -
            right(cable_cutout_center_x)(
                forward(
                    self.main_key_field.thumb_y_indent
                    -
                    self.c.wall_width
                    -
                    self.c.wall_clearance
                    -
                    self.c.back_plate_reduction
                )(
                    cube((
                        self.c.thumb_back_plate_cable_cutout_width,
                        2*self.c.thumb_back_plate_cable_cutout_depth,
                        200
                    ), center=True)
                )
            )
            -
            self.thumb_screw_poles(skip_screws=True)
            -
            self.bottom_screw_connectors
            -
            self.guts.bottom_screws
        )


class FingerCaps(Part):

    def caps_for_column(self, column_ix: int) -> List[caps.FingerCap]:
        """
        Factors key caps for one column.
        :param width:
        :return:
        """
        assert column_ix < self.c.main_field_num_columns
        ## experiment to have filling cap widths, but it does not fully work this way
        #extra_left = 0 if column_ix == 0 else self.c.key_x_extra_gaps[column_ix - 1] / 2
        #extra_right = 0 if column_ix == self.c.main_field_num_columns - 1 else self.c.key_x_extra_gaps[column_ix] / 2
        #width = self.c.key_x_width - self.c.key_cap_gap + extra_left + extra_right
        #x_offset = -extra_left if spec.rotated else extra_left,
        width = self.c.key_x_width - self.c.key_cap_gap
        x_offset = 0

        return [
            caps.FingerCap(
                c=self.c,
                elongation=spec.elongation,
                tilt=spec.tilt,
                extra_height=spec.extra_height,
                width=width,
                length=length - self.c.key_cap_gap,
                x_offset=x_offset,
                rotation=180 if spec.rotated else 0
            )
            for spec, length in zip(self.c.finger_caps, self.c.key_y_widths)
        ]

    def __call__(self):
        cols = self.main_key_field.coordinate_cols()
        return sum([
            sum([
                translate(coord)(
                    rotate((self.c.x_tilt, -self.c.tilt, 0))(up(10)(
                        cap()
                    ))
                ) for coord, cap in zip(col.key_coordinates(), self.caps_for_column(ix)[::-1])
            ]) for ix, col in enumerate(cols)
        ])


class ThumbCaps(Part):

    @property
    def raw_caps(self):
        assert len(self.c.thumb_cluster_radial_positions) > 2, "This keyboard needs at least three thumb keys. :)"
        tkf = self.thumb_key_field

        row_1_left_angles = [self.c.thumb_row_1_1st_cap_left_angle] + [
            (self.c.thumb_cluster_radial_positions[i+1] - self.c.thumb_cluster_radial_positions[i]) / 2
            for i in range(len(self.c.thumb_cluster_radial_positions) - 1)
        ]

        row_1_right_angles = [
            (self.c.thumb_cluster_radial_positions[i + 1] - self.c.thumb_cluster_radial_positions[i]) / 2
            for i in range(len(self.c.thumb_cluster_radial_positions) - 1)
        ] + [self.c.thumb_row_1_last_cap_right_angle]

        result_caps = [
            caps.ThumbRow1Cap(
                self.c,
                radius=self.c.thumb_cluster_radius,
                upper_length=self.c.thumb_row_1_caps_upper_length,
                lower_length=self.c.thumb_row_1_caps_lower_length,
                angle_left=left_angle,
                angle_right=right_angle,
                interface=self.c.cap_interface
            ) for left_angle, right_angle in zip(row_1_left_angles, row_1_right_angles)
        ]
        if len(self.c.thumb_cluster_secondary_radial_positions) > 0:
            angle = self.c.thumb_row_2_caps_angle / 2
            result_caps.extend([
                caps.ThumbRow2Cap(self.c, -angle, angle, self.c.cap_interface)
            ] * 2)
        return result_caps

    def __call__(self):
        return sum(
            [
                translate((p[0], p[1], self.c.thumb_cluster_height + 10))(
                    rotate((0, 0, -p[2]))(
                       cap()
                    )
                )
                for cap, p in zip(self.raw_caps, self.thumb_key_field.positions)
            ]
        )


if __name__ == "__main__":
    pass
