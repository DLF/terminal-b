import math
import numpy as np
from solid import translate, square, rotate, cylinder, circle, cube, color
from solid.utils import back, right, up, arc, forward, down, left

from termb.configuration import Configuration
from termb.generic.flat_head_srew import ScrewFlatHead
from termb.generic.forms import RoundingStamp
from termb.generic.jack import TubeJack
from termb.generic.nut import Nut
from termb.generic.promicro import PromicFrontCutaway
from termb.generic.rotary_encoder import RotaryEncoder


def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    axis = axis/math.sqrt(np.dot(axis, axis))
    a = math.cos(theta/2.0)
    b, c, d = -axis*math.sin(theta/2.0)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])


def get_min_higher(val, value_list):
    """
    For a given value `val` and a list of values, return that value from the list,
    which is the smallest value bigger than `val`.

    If no value in the list is bigger than `val`, `None` is returned.
    Precondition: the value list must be sorted.
    """
    l2 = sorted(value_list)
    assert l2 == value_list, "Value list not sorted"
    for v in value_list:
        if v >= val:
            return v
    return None


class MainKeyField(object):

    class Column(object):
        def __init__(self, c, mkf, x, y_upper, number):
            self.c = c
            self.mkf = mkf
            self.x = x
            self.y_upper = y_upper
            self.number = number

        def raw_key_coordinates(self):
            for ix in range(self.number):
                yield self.x, self.y_upper - self.c.sum_key_y_width_from_top(ix)

        def key_coordinates(self):
            for ix in range(self.number):
                yield self.mkf.transform_surface_point((self.x, self.y_upper - self.c.sum_key_y_width_from_top(ix)))

    def __init__(
            self,
            c: Configuration
    ):
        self.c = c
        self.num_rows = self.c.main_field_num_rows
        self.num_extra_row_cols = self.c.main_field_extra_row_cols
        self._thumb_cluster_x_spacing = self.c.thumb_cluster_x_spacing
        self._thumb_cluster_y_spacing = self.c.thumb_cluster_y_spacing

    def transform_surface_point(self, xy):
        """
        For a given 2D-point `xy` on the finger plate surface, this method returns the 3D-point where `xy` ends up
        in the full model (after tilting and moving the finger plate).
        :param xy: a 2d-point on the finger plate surface
        :return: the position of `xy` in the keyboard model
        """
        v = [xy[0], xy[1], 0]
        x_axis = [1, 0, 0]
        y_axis = [0, 1, 0]
        v2 = np.dot(rotation_matrix(y_axis, math.radians(-self.c.tilt)), v)
        v3 = np.dot(rotation_matrix(x_axis, math.radians(self.c.x_tilt)), v2)
        return v3[0], v3[1], v3[2] + self.c.outer_height

    def z_for_base_coordinate(self, xy):
        t = self.transform_surface_point(xy)
        return t[2]

    def tilt(self, o):
        return rotate((self.c.x_tilt, 0, 0))(rotate((0, -self.c.tilt, 0))(o))

    @property
    def x_width(self):
        """
        X-width of the main key field on its surface.
        """
        return 1 * (
            self.c.main_field_num_columns * self.c.key_x_width
            +
            sum(self.c.key_x_extra_gaps)
            +
            self.c.main_key_field_extra_inner_border
            +
            self.c.main_key_field_extra_outer_border
            +
            2 * self.c.key_field_border
        )

    @property
    def y_width(self):
        """
        Y-width of the main key field on its surface.
        """
        return 1 * (
            self.c.sum_key_y_width_from_bottom(self.num_rows + 1)
            +
            self.c.main_field_col_offsets[0]
            +
            2 * self.c.key_field_border
            +
            self.c.main_key_field_extra_lower_border
            +
            self.c.main_key_field_extra_upper_border
        )

    @property
    def x(self):
        return 0

    @property
    def y(self):
        return 0

    @property
    def shadow_x_width(self):
        """
        X-width of the main key field’s shadow (projection to ground).
        """
        return self.x_width * math.cos(math.radians(self.c.tilt))

    @property
    def shadow_y_width(self):
        """
        Y-width of the main key field’s shadow (projection to ground).
        """
        return self.y_width * math.cos(math.radians(self.c.x_tilt))

    @property
    def sw_corner_center(self):
        """
        The 2D point (on the ground) of the center of the rounding of the lower/inner corner.
        """
        return [self.shadow_x_width - self.c.corner_radius, self.thumb_y_indent + self.c.corner_radius]

    @property
    def footprint(self):
        return translate((self.x, self.y, 0))(
            square([self.shadow_x_width, self.shadow_y_width])
            -
            translate((
                self.thumb_x_indent,
                self.thumb_y_indent,
                0
            ))(
                back(150)(square([150, 150]))
            )
            +
            translate((
                self.thumb_x_indent,
                self.thumb_y_indent,
                0
            ))(
                self.c.thumb_cluster_corner_rounding_stamp(90)
            )
            -
            self.c.corner_rounding_stamp(180)
            -
            translate((0, self.shadow_y_width, 0))(self.c.corner_rounding_stamp(90))
            -
            translate((self.shadow_x_width, self.shadow_y_width, 0))(self.c.corner_rounding_stamp(0))
            -
            translate((self.shadow_x_width, self.thumb_y_indent, 0))(self.c.corner_rounding_stamp(270))
            -
            translate((self.thumb_x_indent, self.y, 0))(self.c.corner_rounding_stamp(270))
        )

    @property
    def thumb_x_indent(self):
        """
        Left X-coordinate where the thumb cluster starts.
        """
        return 1 * (
            #self.c.sum_key_x_width_from_outer(self.num_extra_row_cols)
            self.c.key_x_width * self.c.main_field_extra_row_cols
            +
            sum(self.c.key_x_extra_gaps[:self.c.main_field_extra_row_cols - 1])
            +
            self.c.key_field_border
            +
            self._thumb_cluster_x_spacing
        )

    @property
    def lowest_y_on_short_colums(self):
        short_coordinate_colums = list(self.coordinate_cols())[self.c.main_field_extra_row_cols:]
        lowest_coordinates = [list(col.raw_key_coordinates())[-1] for col in short_coordinate_colums]
        min_y_key_coords = None
        for coord in lowest_coordinates:
            if min_y_key_coords is None or coord[1] <= min_y_key_coords[1]:
                min_y_key_coords = coord
        transformed_min_point = self.transform_surface_point((
            min_y_key_coords[0],
            1 * (
                min_y_key_coords[1]
                - self.c.key_y_widths[1]/2
                - self.c.key_field_border
                - self.c.thumb_cluster_y_spacing
            )
        ))
        return transformed_min_point[1]

    @property
    def thumb_y_indent(self):
        """
        Upper Y-coordinate where the thumb cluster starts.
        """
        return self.lowest_y_on_short_colums

    @property
    def thumb_indent(self):
        return (
            self.thumb_x_indent,
            self.thumb_y_indent
        )

    def coordinate_cols(self):
        for col_ix in range(self.c.main_field_num_columns):
            yield (
                MainKeyField.Column(
                    c=self.c,
                    mkf=self,
                    x=1*(
                        self.c.key_x_center_from_outer(col_ix)
                        + self.c.main_key_field_extra_outer_border
                        + self.c.key_field_border
                    ),
                    y_upper=1*(
                        self.y_width
                        - self.c.key_field_border
                        - self.c.key_y_widths[-1]/2
                        - self.c.main_field_col_offsets[col_ix]
                        - self.c.main_key_field_extra_upper_border
                    ),
                    number=self.num_rows + 1 if col_ix < self.num_extra_row_cols else self.num_rows
                )
            )

    def apply_for_each(self, scad_object):
        return sum(
            [
                translate(coord)(
                    self.tilt(scad_object)
                ) for col in self.coordinate_cols() for coord in col.key_coordinates()
            ]
        )

    @property
    def as_markers(self):
        return self.apply_for_each(cylinder(r=1, h=10))

    @property
    def as_stamps(self):
        return self.apply_for_each(self.c.switch_stamp)

    @property
    def screw_positions(self):
        lower_x = self.c.wall_width + self.c.screw_side_distance + 0.5 * self.c.screw_hole_diameter
        lower_y = lower_x
        upper_y = self.shadow_y_width - lower_y
        upper_x = self.shadow_x_width - lower_x
        return [
            ScrewPosition(lower_x, lower_y, 0),
            ScrewPosition(lower_x, upper_y, 270),
            ScrewPosition(upper_x, lower_y + self.thumb_y_indent, 90),
            ScrewPosition(upper_x, upper_y, 180),
            ScrewPosition(self.thumb_x_indent - lower_x, lower_y, 90)
        ]

    def print_screw_lengths(self):

        for p, d, caption in zip(
            self.screw_positions,
            self.c.nut_depths_main_part,
            ["outer lower", "outer upper", "inner lower", "inner upper", "middle"]
        ):
            lengths = self.c.available_screw_lengths
            xy = self.lowest_screw_head_point(p.c)
            h = self.z_for_base_coordinate(xy) - self.c.screw_head_extra_indent
            min_length = h - d + self.c.nut_height
            length = get_min_higher(min_length, lengths)
            print("Length of finger plate {} screw: {} mm {}".format(
                caption,
                str(length),
                "" if length is None else "({} mm excess)".format(length - min_length),
            ))

    def lowest_screw_head_point(self, xy):
        """
        This calculation is only an approximation.
        The returned point is assumed to be the “sufficiently” lowest point of the screw head’s outer ring.
        :param xy: center xy point of a screw
        :return: xy of screw head at the lowest finger plate point
        """
        r = self.c.screw_head_hole_diameter / 2
        vx = self.c.tilt
        vy = self.c.x_tilt
        rad = 0 if vx + vy == 0 else math.asin(vx / (vx + vy))
        x = r * math.sin(rad)
        y = r * math.cos(rad)
        return xy[0] - x, xy[1] - y

    @property
    def screw_translations(self):
        result = []
        for p in self.screw_positions:
            xy = self.lowest_screw_head_point(p.c)
            h = self.z_for_base_coordinate(xy) - self.c.screw_head_extra_indent
            result.append(
                (
                    p.x,
                    p.y,
                    h
                )
            )
        return result

    def nob_translate(self, o):
        x = self.thumb_x_indent - self.c.decoration_finger_rim
        y = self.thumb_y_indent
        return translate((x, y, 0))(
            right(self.c.decoration_nob_plate_x_length/2)(o)
        )

    @property
    def back_plate_screw_positions(self):
        result = [
            # inner
            ScrewPosition(
                x=1*(
                    self.x_width
                    -
                    self.c.wall_width
                    -
                    (
                        self.c.main_key_field_extra_inner_border + self.c.key_field_border
                    ) / 2
                ),
                y=self.y_width - (
                    (self.y_width - self.thumb_y_indent) / 2
                ),
                angle=180
            )
            ,
            # lower
            ScrewPosition(
                x=1*(
                    self.c.key_field_border
                    +
                    self.c.main_key_field_extra_outer_border
                    +
                    self.c.key_x_center_from_outer(self.c.main_field_extra_row_cols - 1)
                ),
                y=1*(
                    self.c.wall_width
                    +
                    self.c.screw_side_distance
                    +
                    self.c.screw_hole_diameter
                    +
                    self.c.screw_pole_thickness
                    +
                    self.c.back_plate_rounding
                    +
                    0.5 * self.c.back_plate_screw_block_thickness
                ),
                angle=90
            )
            ,
            # upper
            ScrewPosition(
                x=1 * (
                    self.c.key_field_border
                    +
                    self.c.main_key_field_extra_outer_border
                    +
                    1.5 * self.c.key_x_width
                    +
                    self.c.key_x_extra_gaps[0]
                    +
                    self.c.finger_plate_upper_block_x_offset
                ),
                y=1 * (
                    self.y_width
                    -
                    self.c.wall_width
                    -
                    (
                        self.c.key_field_border
                        +
                        self.c.main_field_col_offsets[1]
                        -
                        self.c.wall_width
                    ) / 2
                ),
                angle=270
            )
        ]
        return result

    def for_all_back_plate_screw_positions(self, o):
        return sum([
            translate(self.transform_surface_point(p.c))(
                rotate((self.c.x_tilt, 0, 0))(
                    rotate((0, -self.c.tilt, 0))(
                        rotate((0, 0, p.angle))(o)
                    )
                )
            ) for p in self.back_plate_screw_positions
        ])


    @property
    def _upper_encoder_coord(self):
        x = self.c.upper_encoder_x_distance
        yd = self.c.upper_ecoder_y_distance
        if x is None or yd is None:
            return None
        y = self.y_width - yd
        coord = self.transform_surface_point((x, y))
        return coord

    @property
    def upper_encoder(self):
        encoder = down(self.c.switch_plate_thickness)(
            rotate(180)(RotaryEncoder(self.c.encoder_config())())
        )
        return translate(self._upper_encoder_coord)(
            self.tilt(
                encoder
            )
        )

    @property
    def upper_ecoder_socket_block(self):
        socket_dim = self.c.upper_encoder_socket_dimension
        socket = down(socket_dim[2]/2 + self.c.switch_plate_thickness)(
            cube(socket_dim, center=True)
        )
        return translate(self._upper_encoder_coord)(
            self.tilt(
                color("Tomato")(socket)
            )
        )

    @property
    def upper_ecoder_nob(self):
        """
        Only used for visualization
        """
        return translate(self._upper_encoder_coord)(
            self.tilt(
                color("blue")(
                    cylinder(d=self.c.encoder_nob_diameter, h=self.c.encoder_nob_height, segments=30)
                )
            )
        )


class ThumbKeyField(object):

    def __init__(self, configuration: Configuration, main_key_field):
        self.c = configuration
        self.xy_offset = main_key_field.thumb_indent
        self._main_key_field = main_key_field

    def get_radial_point(self, angle, radius):
        x = math.sin(math.radians(angle)) * radius + self.rectangular_part_x_width_with_roundings + self.xy_offset[0]
        y = math.cos(math.radians(angle)) * radius - self.outer_radius + self.xy_offset[1]
        return x, y

    @property
    def positions(self):
        result = []
        for angle in self.c.thumb_cluster_radial_positions:
            x = math.sin(math.radians(angle)) * self.c.thumb_cluster_radius
            y = math.cos(math.radians(angle)) * self.c.thumb_cluster_radius - self.outer_radius
            result.append((x, y, angle))
        for angle in self.c.thumb_cluster_secondary_radial_positions:
            x = math.sin(math.radians(angle)) * self.c.thumb_cluster_secondary_radius
            y = math.cos(math.radians(angle)) * self.c.thumb_cluster_secondary_radius - self.outer_radius
            result.append((x, y, angle))
        return [
            (
                r[0] + self.rectangular_part_x_width_with_roundings + self.xy_offset[0],
                r[1] + self.xy_offset[1],
                r[2]
            ) for r in result
        ]

    @property
    def screw_positions(self):
        end_box_offset = self.c.wall_width + self.c.screw_side_distance + 0.5 * self.c.screw_hole_diameter
        pole_offset = self.c.wall_width + self.c.screw_side_distance + 0.5 * self.c.screw_hole_diameter
        x_upper_right, y_upper_right = self.end_box_dimensions
        x_upper_right -= end_box_offset
        y_upper_right -= end_box_offset
        x_lower_right = x_upper_right
        y_lower_right = end_box_offset

        x_upper_right, y_upper_right = self.end_box_point_translate(x_upper_right, y_upper_right)
        x_lower_right, y_lower_right = self.end_box_point_translate(x_lower_right, y_lower_right)
        return [
            ScrewPosition(
                self.xy_offset[0] + pole_offset,
                self.xy_offset[1] - pole_offset,
                270
            ),
            ScrewPosition(
                self.xy_offset[0] + pole_offset,
                self.xy_offset[1] - self.y_width_with_roundings + pole_offset,
                0
            ),
            ScrewPosition(
                self._main_key_field.shadow_x_width - self.c.screw_hole_diameter,
                self.xy_offset[1] - pole_offset,
                225
            ),
            ScrewPosition(
                x_upper_right,
                y_upper_right,
                180 - self.c.thumb_cluster_end_angle
            ),
            ScrewPosition(
                x_lower_right,
                y_lower_right,
                90 - self.c.thumb_cluster_end_angle
            ),
        ]

    @property
    def screw_translations(self):
        return [
            (
                p.x, p.y, self.c.thumb_cluster_height - self.c.screw_head_extra_indent
            ) for p in self.screw_positions
        ]

    @property
    def screw_length(self):
        return 1 * (
            self.c.thumb_cluster_height
            -
            self.c.screw_head_extra_indent
            -
            self.c.nut_depth_thumb_cluster
            +
            self.c.nut_height
        )

    def print_screw_length(self):
        min_length = self.screw_length
        length = get_min_higher(min_length, self.c.available_screw_lengths)
        excess = None if length is None else length - min_length
        print("Length of thumb plate screws: {} mm ({} mm excess)".format(length, excess))

    @property
    def y_width_with_roundings(self):
        return self.outer_radius - self.inner_radius
        #return max(
        #    2 * self.c.key_field_border
        #    +
        #    self.c.thumb_key_field_extra_top_border
        #    +
        #    self.c.key_square_width
        #    +
        #    self.c.thumb_key_field_extra_lower_border,
        #    self.outer_radius - self.inner_radius
        #)

    @property
    def outer_radius(self):
        return 1 * (
            self.c.thumb_cluster_radius
            + self.c.thumb_key_field_extra_top_border
        )

    @property
    def inner_radius(self):
        r = self.c.thumb_cluster_radius \
            if len(self.c.thumb_cluster_secondary_radial_positions) == 0 \
            else self.c.thumb_cluster_secondary_radius
        return 1 * (
            r
            - self.c.thumb_key_field_extra_lower_border
        )

    @property
    def rectangular_part_x_width_with_roundings(self):
        return self.c.thumb_cluster_center_x

    def apply_for_each(self, scad_object):
        return sum([translate((p[0], p[1], self.c.thumb_cluster_height))(rotate(-p[2])(scad_object)) for p in self.positions])

    @property
    def as_markers(self):
        return self.apply_for_each(cylinder(r=1, h=10))

    @property
    def as_stamps(self):
        return self.apply_for_each(self.c.switch_stamp)

    @property
    def _thumb_arc_center(self):
        return [
            self.xy_offset[0] + self.rectangular_part_x_width_with_roundings,
            self.xy_offset[1] - self.outer_radius
        ]

    @property
    def y_excess(self):
        return self.y_width_with_roundings - self.xy_offset[1]

    @property
    def bridge_rounding_radius(self):
        return min(self.y_excess / 2, self.c.thumb_cluster_bridge_max_rounding_radius)

    @property
    def footprint(self):
        # calculating radius for the rounded cutaway on the upper left
        p1 = self._thumb_arc_center
        p2 = self._main_key_field.sw_corner_center
        p2[0] += self.c.corner_radius
        a = p2[0] - p1[0]
        b = self.outer_radius
        f = p2[1] - p1[1]
        r = (a ** 2 - b ** 2 + f ** 2) / (-2 * a + 2 * b)
        # calculating the center of the cutaway circle
        cutaway_center = (
            p2[0] + r,
            p2[1],
            0
        )
        # calculating touch point with outer radius
        radius_ratio = self.outer_radius / (self.outer_radius + r)
        touch_point = [
            p1[0] + (cutaway_center[0] - p1[0]) * radius_ratio,
            p1[1] + (cutaway_center[1] - p1[1]) * radius_ratio
        ]
        # calculating rectangular base plate origin for the rounding
        rounding_base_position = (
            self.xy_offset[0] + self.rectangular_part_x_width_with_roundings,
            touch_point[1],
            0
        )
        # calculating rectangular base plate width and height for the rounding
        rounding_base_dimension = [
            touch_point[0] - rounding_base_position[0],
            p2[1] - rounding_base_position[1]
        ]

        end_box_dimensions = self.end_box_dimensions

        excess_corner_rounding = RoundingStamp(self.bridge_rounding_radius)

        return up(0)(
            translate(self.xy_offset)(
                right(self.rectangular_part_x_width_with_roundings)(
                    # The square part.
                    # We have this c.corner_radius to get the additional overlap with the rounded corner of
                    # the main part on the left.
                    translate((
                        -self.rectangular_part_x_width_with_roundings - self.c.corner_radius + self.bridge_rounding_radius,
                        -self.y_width_with_roundings,
                        0
                    ))(
                        square([
                            self.rectangular_part_x_width_with_roundings + self.c.corner_radius - self.bridge_rounding_radius,
                            self.y_width_with_roundings
                        ])
                    )
                    +
                    # The radial part.
                    back(self.outer_radius)(
                        arc(
                            rad=self.outer_radius,
                            end_degrees=90,
                            start_degrees=90 - self.c.thumb_cluster_end_angle,
                            segments=200
                        )
                        -
                        circle(r=self.inner_radius, segments=200)
                    )
                )
            )
            +
            # the rounded filling on the inner top
            translate(rounding_base_position)(
                square(rounding_base_dimension)
            )
            -
            translate(cutaway_center)(
                circle(r, segments=200)
            )
            +
            # the square end-form (towards the inside)
            self.end_box_translate(
                square(end_box_dimensions)
                -
                translate((end_box_dimensions[0], end_box_dimensions[1], 0))((self.c.corner_rounding_stamp()))
                -
                translate((end_box_dimensions[0], 0, 0))(
                    rotate(270)(
                        self.c.corner_rounding_stamp()
                    )
                )
            )
            +
            # concave rounding towards the outside
            right(self.xy_offset[0] - self.c.corner_radius)(
                back(self.bridge_rounding_radius)(
                    square(self.bridge_rounding_radius)
                    -
                    circle(r=self.bridge_rounding_radius, segments=100)
                )
                +
                square(self.bridge_rounding_radius)  # just a filler to the rounding of the main part
            )
            -
            # convex rouding
            right(self.xy_offset[0] - self.c.corner_radius + self.bridge_rounding_radius)(
                back(self.y_excess)(
                    rotate(180)(excess_corner_rounding())
                )
            )
        )

    def end_box_translate(self, o):
        end_angle = self.c.thumb_cluster_end_angle
        end_box_origin = self.get_radial_point(end_angle, self.inner_radius)
        # We add this tiny 0.0001 to each coord to make the end-box properly union with the
        # radial part. Otherwise, the footprint breaks apart when using a negativ offset operation.
        return translate((end_box_origin[0] - 0.0001, end_box_origin[1] + 0.0001, 0))(
            rotate((0, 0, -end_angle))(
                o
            )
        )

    def end_box_point_translate(self, x, y):
        end_angle = self.c.thumb_cluster_end_angle
        end_box_origin = self.get_radial_point(end_angle, self.inner_radius)
        xx, yy = rotate_point(x, y, end_angle)
        return xx + end_box_origin[0], yy + end_box_origin[1]

    @property
    def end_box_dimensions(self):
        end_box_x = self.c.corner_radius + self.c.thumb_outer_box_extra_width
        end_box_y = self.outer_radius - self.inner_radius
        return end_box_x, end_box_y

    def nob_translate(self, o):
        p = self.get_radial_point(self.c.decoration_thumb_nob_angle, self.inner_radius)
        return translate(p)(
            rotate((0, 0, -self.c.decoration_thumb_nob_angle))(
                o
            )
        )

    def for_all_back_plate_screw_positions(self, o):
        if len(self.c.thumb_cluster_secondary_radial_positions) == 0:
            return 0
        return sum([
            translate(p.c)(
                up(self.c.thumb_cluster_height)(
                    rotate((0, 0, p.angle))(o)
                )
            ) for p in self.back_plate_screw_positions
        ])

    @property
    def back_plate_screw_positions(self):
        r_outer = 1 * (
            self.inner_radius
            +
            self.c.wall_width
            +
            self.c.screw_side_distance
            +
            self.c.screw_hole_diameter
            +
            self.c.screw_pole_thickness
            +
            self.c.back_plate_screw_block_base_width / 2
            +
            self.c.thumb_back_plate_inner_screw_rad_offset
        )
        end_angle = self.c.thumb_cluster_end_angle
        end_box_width, _ = self.end_box_dimensions
        inner_screw_base_point = self.get_radial_point(end_angle, r_outer)

        result = [
            ScrewPosition(
                x=1*(
                    self.xy_offset[0]
                    +
                    self.c.wall_width
                    +
                    self.c.back_plate_screw_block_thickness / 2
                    +
                    self.c.thumb_back_plate_outer_screw_x_offset
                ),
                y=self.xy_offset[1] - self.y_width_with_roundings / 2,
                angle=0
            ),
            ScrewPosition(
                x=inner_screw_base_point[0],
                y=inner_screw_base_point[1],
                angle=180-end_angle
            )
        ]
        return result


class Promic:

    def __init__(self, configuration, main_key_field):
        self.c = configuration
        self.main_key_field = main_key_field

    @property
    def usb_front_plate_width(self):
        return self.c.promic_config.promic_width + 2 * self.c.promic_config.notch_bar_width

    @property
    def usb_front_plate_deco_notch_marker_without_clearance(self):
        return self.get_usb_front_plate_notch_marker(
            2*self.c.decoration_base_usb_plate_notch_depth,
            0,
            "red"
        )

    @property
    def usb_front_plate_deco_notch_marker_with_clearance(self):
        return self.get_usb_front_plate_notch_marker(
            2*self.c.decoration_base_usb_plate_notch_depth - 2*self.c.decoration_base_usb_plate_side_clearance,
            0,
            "red"
        )

    @property
    def usb_front_plate_notch_marker_without_clearance(self):
        return self.get_usb_front_plate_notch_marker(
            2*self.c.usb_connector_notch_depth,
            self.c.usb_connector_notch_depth,
            "red"
        )

    @property
    def usb_front_plate_notch_marker_with_clearance(self):
        return self.get_usb_front_plate_notch_marker(
            2*self.c.usb_connector_notch_depth - 2*self.c.usb_connector_notch_side_clearance,
            self.c.usb_connector_notch_depth - self.c.usb_connector_notch_top_clearance,
            "red"
        )

    def get_usb_front_plate_notch_marker(self, side_notch, top_notch, colorization=None):
        width = self.usb_front_plate_width - side_notch
        height = self.c.promic_usb_plate_height - top_notch
        # this fancy calculation is only for debug purposes
        overhang_x = 1 + max(side_notch, 0) + max(top_notch, 0)
        marker = (
            self.transition(
                forward(-width / 2)(
                    down(self.c.promic_plate_thickness)(
                        left(self.c.promic_usb_plate_thickness + overhang_x)(
                            cube((
                                self.c.promic_usb_plate_thickness + 2 * overhang_x,
                                width,
                                height
                            ))
                        )
                    )
                )
            )
        )
        if colorization is not None:
            marker = color(colorization)(marker)
        return marker

    @property
    def max_x(self):
        return 1*(
                self.main_key_field.shadow_x_width
                - self.c.promic_indent
        )

    @property
    def center_x(self):
        return 1*(
            self.max_x
            - self.c.promic_config.promic_width / 2
            - self.c.promic_config.notch_bar_width
        )

    def transition(self, o):
        # return up(0)(o)
        return up(self.c.base_plate_thickness + self.c.promic_plate_thickness)(
            right(
                self.center_x
            )(
                forward(
                    self.main_key_field.shadow_y_width
                    +
                    self.c.usb_connector_outer_excess
                    #- self.c.wall_width
                )(
                    rotate(270)(
                        right(self.c.promic_usb_plate_thickness)(
                            o
                        )
                    )
                )
            )
        )

    @property
    def cutaway(self):
        return self.transition(PromicFrontCutaway(self.c.promic_config)())

    @property
    def _bar_screw_x_offset(self):
        return 1 * (
                self.c.promic_config.promic_length
                + self.c.promic_back_bar_width / 2
                - self.c.promic_config.front_notch_depth
        )

    @property
    def _raw_back_screw(self):
        s = ScrewFlatHead(
            d=self.c.screw_hole_diameter,
            dh=self.c.screw_head_hole_diameter,
            ah=90,
            l=self.c.screw_length_promic_back_bar,
            head_elongation=0.1 + self.c.bottom_screw_indent,
            segments=60
        )
        n = Nut(self.c.nut_width, self.c.nut_height + 2*0.5)
        return \
            right(self._bar_screw_x_offset)(
                down(self.c.base_plate_thickness + self.c.promic_plate_thickness - self.c.bottom_screw_indent)(
                    rotate((180, 0, 0))(
                        s()
                    )
                    +
                    up(self.c.promic_back_bar_height - 0.5)(
                        n()
                    )
                )
            )

    @property
    def side_screw_position(self):
        x = 1 * (
            self.c.bottom_connector_diameter / 2
            + self.c.wall_clearance
            - self.c.promic_usb_plate_thickness
            + self.c.usb_connector_outer_excess
            + self.c.wall_width
        )
        y = 1 * (
            self.c.promic_config.promic_width / 2
            + self.c.bottom_connector_diameter / 2
            + self.c.promic_config.notch_bar_width
        )
        return x, y

    @property
    def _raw_side_screw(self):
        s = ScrewFlatHead(
            d=self.c.screw_hole_diameter,
            dh=self.c.screw_head_hole_diameter,
            ah=90,
            l=self.c.screw_length_promic_side,
            head_elongation=0.1 + self.c.bottom_screw_indent,
            segments=60
        )
        n = Nut(self.c.nut_width, self.c.nut_height + 2*0.5)
        x, y = self.side_screw_position
        return \
            right(x)(
                back(y)(
                    down(self.c.base_plate_thickness + self.c.promic_plate_thickness - self.c.bottom_screw_indent)(
                        rotate((180, 0, 0))(
                            s()
                        )
                        +
                        up(self.c.screw_length_promic_side - 0.5 - self.c.nut_height)(
                            n()
                        )
                    )
                )
            )

    @property
    def back_screw(self):
        return self.transition(self._raw_back_screw)

    @property
    def side_screw(self):
        return self.transition(self._raw_side_screw)


class Guts:

    def __init__(
            self,
            configuration: Configuration,
            main_key_field: MainKeyField,
            thumb_key_field: ThumbKeyField,
            promic: Promic
    ):
        self.c = configuration
        self.main_key_field = main_key_field
        self.thumb_key_field = thumb_key_field
        self.jack_model = TubeJack(
            diameter_tube=self.c.jack_diameter_tube,
            diameter_thread=self.c.jack_diameter_thread,
            length_tube=self.c.jack_length_tube,
            length_thread=self.c.jack_length_thread,
            diameter_nut=self.c.jack_diameter_nut,
            length_nut=self.c.jack_length_nut
        )
        self.promic = promic

    @property
    def exit_point(self):
        return (
            self.main_key_field.shadow_x_width - self.c.jack_indent,
            self.main_key_field.screw_positions[2].y
            +
            self.c.screw_hole_diameter/2
            +
            self.c.screw_pole_base_thickness
            +
            self.jack_housing_y_length/2
            + self.c.jack_to_pole_clearance
            ,
            self.c.jack_position_height
        )

    @property
    def jack(self):
        return translate(self.exit_point)(
            rotate((0, 0, 180))(
                self.jack_model()
            )
        )

    @property
    def jack_housing_x_length(self):
        return min(
            self.main_key_field.shadow_x_width - self.c.wall_width - self.promic.max_x,
            self.c.jack_housing_length
        )

    @property
    def jack_housing_y_length(self):
        return self.jack_model.diameter_tube + 2*self.c.jack_wall_thickness

    @property
    def housing_screw_position(self):
        exit_point = self.exit_point
        x = exit_point[0] - self.jack_housing_x_length / 2
        y = exit_point[1] + self.jack_housing_y_length / 2 + self.jack_housing_x_length / 2
        return ScrewPosition(x, y, 180)

    @property
    def extra_bottom_screw_positions(self):
        thumb_connector_radius = self.thumb_key_field.inner_radius + self.c.bottom_connector_diameter / 2 + self.c.wall_width
        thumb_connector_position = self.thumb_key_field.get_radial_point(
            self.c.thumb_cluster_bottom_connector_angle,
            thumb_connector_radius
        )
        result = [
            ScrewPosition(
                self.main_key_field.shadow_x_width / 2 + self.c.top_bottom_connector_x_offset,
                self.main_key_field.shadow_y_width - self.c.wall_width - self.c.bottom_connector_diameter / 2,
                270
            ),
            ScrewPosition(
                self.c.wall_width + self.c.bottom_connector_diameter / 2,
                self.main_key_field.shadow_y_width / 2,
                0
            ),
            ScrewPosition(
                thumb_connector_position[0],
                thumb_connector_position[1],
                90 - self.c.thumb_cluster_bottom_connector_angle
            )

        ]
        return result

    @property
    def _raw_bottom_screw(self):
        s = ScrewFlatHead(
            d=self.c.screw_hole_diameter,
            dh=self.c.screw_head_hole_diameter,
            ah=90,
            l=self.c.screw_length_bottom_connect,
            head_elongation=0.1 + self.c.bottom_screw_indent,
            segments=60
        )
        n = Nut(self.c.nut_width, self.c.nut_height + 0.1)
        return up(0)(
            up(self.c.bottom_screw_indent)(
                rotate((180, 0, 0))(
                    s()
                )
            )
            +
            up(self.c.base_plate_thickness + self.c.bottom_connector_height - self.c.nut_height/2)(
                n()
            )
        )

    @property
    def all_screw_positions(self):
        return self.extra_bottom_screw_positions + [self.housing_screw_position]

    @property
    def bottom_screws(self):
        s = self._raw_bottom_screw
        return sum([
            translate(p.c)(s) for p in self.all_screw_positions
        ])


class ScrewPosition:
    """
    angle: 0 is upper right, runs counter clockwise
    """

    def __init__(self, x, y, angle):
        self.x = x
        self.y = y
        self.angle = angle

    @property
    def c(self):
        return self.x, self.y, 0


def rotate_point(x, y, angle):
    xx = x * math.cos(math.radians(angle)) + y * math.sin(math.radians(angle))
    yy = -x * math.sin(math.radians(angle)) + y * math.cos(math.radians(angle))
    return xx, yy