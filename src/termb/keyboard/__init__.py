import os
from solid import *
from solid.utils import *

from termb.configuration import Configuration
from termb.keyboard.parts import (
    FingerPlate,
    Middle,
    BasePlate,
    ThumbPlate,
    PromicMountPlate,
    PromicBackBar,
    BaseDeco,
    FingerDeco,
    ThumbDeco,
    FingerBackPlate,
    ThumbBackPlate,
    ThumbCaps,
    FingerCaps
)
from termb.keyboard.kernel import MainKeyField, ThumbKeyField, ScrewPosition
from termb.generic.nut import Nut
from termb.generic.flat_head_srew import ScrewFlatHead
from termb.generic.rotary_encoder import RotaryEncoder
from termb.generic.rotary_knob import RotaryKnob
from termb.generic.flattened_shaft import FlattenedShaftCutaway
from termb.keyboard.cap_splitter import Simple


class Renderer:
    def __init__(self, output_prefix):
        self.output_prefix = output_prefix

    def __call__(self, scad_object, base, name):
        path = os.path.join(self.output_prefix, base)
        if not os.path.isdir(path):
            os.makedirs(path)
        scad_render_to_file(scad_object, os.path.join(path, name + ".scad"))


class CapRenderer:
    def __init__(self, renderer: Renderer, cap_splitter):
        self.renderer = renderer
        self.cap_splitter = cap_splitter

    def __call__(self, cap_scad_object, base, name):
        splits = self.cap_splitter(cap_scad_object)
        for part_name, cap_part in splits:
            self.renderer(
                cap_part,
                base,
                name + "_" + part_name
            )


class TerminalB:
    def __init__(self, configuration: Configuration):
        self.c = configuration

        self.finger_plate = FingerPlate(configuration)
        self.middle = Middle(configuration)
        self.base_plate = BasePlate(configuration)
        self.thumb_plate = ThumbPlate(configuration)
        self.promic_mount_plate = PromicMountPlate(configuration)
        self.promic_back_bar = PromicBackBar(configuration)
        self.deco_base = BaseDeco(configuration)
        self.deco_finger = FingerDeco(configuration)
        self.deco_thumb = ThumbDeco(configuration)
        self.finger_back_plate = FingerBackPlate(configuration)
        self.thumb_back_plate = ThumbBackPlate(configuration)

        self.thumb_caps = ThumbCaps(configuration)
        self.finger_caps = FingerCaps(configuration)

        print("------------------------------------------------------------------")
        print("Rendering  ** {} **".format(self.c.name))
        print("------------------------------------------------------------------")
        self.finger_plate.main_key_field.print_screw_lengths()
        self.finger_plate.thumb_key_field.print_screw_length()
        # Options for M3 screws:
        # 5, 6, 8, 10, 12, 14, 16, 18, 20, 22, 25, 30

    @property
    def position_marker(self):
        return up(0)(
            color("red")(sphere(r=0.3, segments=20))
            +
            color("pink")(cylinder(r=0.1, h=5))
            +
            color("thistle")(linear_extrude(0.1)(
                circle(r=2, segments=10) - circle(r=1.5, segments=10)
            ))
            +
            color("plum")(linear_extrude(0.075)(
                circle(r=2.5, segments=10) - circle(r=2, segments=10)
            ))
            +
            color("violet")(linear_extrude(0.05)(
                circle(r=3, segments=10) - circle(r=2.5, segments=10)
            ))
            +
            down(0.1 + 0.1)(color("orange")(linear_extrude(0.1)(
                circle(r=5, segments=4) - circle(r=4.5, segments=4)
            )))
            +
            down(0.1 + 0.075)(color("pink")(linear_extrude(0.1)(
                circle(r=5.5, segments=4) - circle(r=5, segments=4)
            )))
            +
            down(0.1 + 0.05)(color("yellow")(linear_extrude(0.1)(
                circle(r=6, segments=4) - circle(r=5.5, segments=4)
            )))
        )

    @property
    def switch_postion_markers(self):
        return self.middle.main_key_field.apply_for_each(
            self.position_marker
        )

    @property
    def switch_position_cube_test(self):
        return up(0)(
            cube((150, 120, 100))
            -
            self.middle.upper_main_cutaway
            +
            self.switch_postion_markers
        )

    @property
    def surface_transform_test(self):
        w = 20 * 20
        return up(0)(
            up(self.c.outer_height)(
                rotate((self.c.x_tilt, 0, 0))(
                    rotate((0, -self.c.tilt, 0))(
                        down(1)(color("Navy")(cube((w, w, 1))))
                    )
                )
            )
            +
            sum([
                translate(self.middle.main_key_field.transform_surface_point((x, y)))(
                    color("orange")(sphere(r=0.2 if (x+y) % 40.0 == 0 else 1, segments=50))
                    +
                    rotate((self.c.x_tilt, 0, 0))(
                        rotate((0, -self.c.tilt, 0))(
                            self.position_marker
                        )
                    )
                )
                for x in range(0, w+20, 20) for y in range(0, w+20, 20)
            ])
        )

    @property
    def screw_pole_test_print(self):
        screw_height = 20
        height = screw_height + 2.2
        nut_height = 4.8
        pole = self.base_plate.poles(
            [ScrewPosition(0, 0, 0)],
            [nut_height + self.c.screw_pole_base_height_above_nut],
            minkowski()(
                square((40, 40)),
                circle(self.c.corner_radius)
            )
        )
        pole = intersection()(
            pole,
            up(-50+height)(cube((100, 100, 100), center=True))
        )
        nut = Nut(self.c.nut_width + self.c.bottom_nut_extra_clearance, nut_height + 0.1)()
        screw = ScrewFlatHead(
            d=self.c.screw_hole_diameter,
            dh=self.c.screw_head_hole_diameter,
            ah=90,
            l=self.c.screw_length_bottom_connect,
            head_elongation=30,
            segments=60
        )()
        return up(0)(
            pole
            -
            down(0.1)(nut)
            -
            up(height - self.c.screw_head_extra_indent)(screw)
        )

    @property
    def rotary_knob(self):
        knob = RotaryKnob(self.c.encoder_knob_config)()
        shaft = FlattenedShaftCutaway(self.c.encoder_shaft_config)()
        return knob - down(self.c.knob_to_shaft_base_distance)(shaft)

    def render(self):
        output_dir = "../scad/{}".format(self.c.name)
        r = Renderer(output_dir)
        self._render_parts(r)
        self._render_dev_views(r)
        self._render_visualizations(r)
        self._render_prints(r)
        self._render_test_prints(r)

    def _render_parts(self, r):
        keyboard = self
        r(keyboard.finger_plate(), "part", "finger_plate")
        r(keyboard.middle(), "part", "middle")
        r(keyboard.base_plate(), "part", "base_plate")
        r(keyboard.thumb_plate(), "part", "thumb_plate")
        r(keyboard.promic_mount_plate(), "part", "promic_mount_plate")
        r(keyboard.promic_back_bar(), "part", "promic_back_bar")
        r(keyboard.finger_back_plate(), "part", "finger_back_plate")
        r(keyboard.thumb_back_plate(), "part", "thumb_back_plate")
        r(keyboard.deco_base(), "part", "deco_base")
        r(keyboard.deco_finger(), "part", "deco_finger")
        r(keyboard.deco_thumb(), "part", "deco_thumb")

    def _render_prints(self, ren):

        class RendererWrapper:
            def __init__(self, renderer: Renderer, name_prefix):
                self.renderer = renderer
                self.name_prefix = name_prefix

            def __call__(self, scad_object, base, name):
                self.renderer(scad_object, base, self.name_prefix + name)

        keyboard = self
        splitter = [Simple()] if self.c.key_cap_splitter is None else self.c.key_cap_splitter
        for side_name, transformer in [
            ("left", lambda o: o),
            ("right", lambda o: mirror((1, 0, 0))(o))
        ]:
            subdir = os.path.join("print", side_name)

            r = RendererWrapper(ren, side_name + "-")

            cap_renderer = [CapRenderer(r, s) for s in splitter]

            for cap_r in cap_renderer:
                cap_name = cap_r.cap_splitter.name
                subsubdir = os.path.join(subdir, "caps", cap_name)
                for ix, thumb_cap in enumerate(keyboard.thumb_caps.raw_caps):
                    cap_r(
                        thumb_cap(),
                        subsubdir,
                        "thumb_cap_{}".format(ix)
                    )
                for col_ix in range(self.c.main_field_num_columns):
                    for row_ix, cap in enumerate(keyboard.finger_caps.caps_for_column(col_ix)):
                        cap_r(
                            cap(),
                            subsubdir,
                            "finger_cap_c{col_ix}_r{row_ix}".format(row_ix=row_ix, col_ix=col_ix)
                        )
            r(
                transformer(
                    rotate((0, self.c.tilt - 180, 0))(rotate((-self.c.x_tilt, 0, 0))(
                        keyboard.finger_plate()
                    ))
                ),
                subdir, "finger_plate"
            )
            r(
                transformer(keyboard.middle()), subdir, "middle"
            )
            r(
                transformer(keyboard.base_plate()), subdir, "base_plate"
            )
            r(
                transformer(
                    rotate((0, 180, 0))(
                        left(keyboard.finger_plate.main_key_field.thumb_x_indent)(
                            keyboard.thumb_plate()
                        )
                    )
                ), subdir, "thumb_plate"
            )
            r(
                transformer(keyboard.promic_mount_plate()), subdir, "promic_mount_plate"
            )
            r(
                transformer(keyboard.promic_back_bar()), subdir, "promic_back_bar"
            )
            r(
                transformer(
                    rotate((0, self.c.tilt - 180, 0))(rotate((-self.c.x_tilt, 0, 0))(
                        keyboard.finger_back_plate()
                    ))
                ), subdir, "finger_back_plate"
            )
            r(
                transformer(
                    rotate((0, 180, 0))(
                        left(keyboard.finger_plate.main_key_field.thumb_x_indent)(
                            keyboard.thumb_back_plate()
                        )
                    )
                ), subdir, "thumb_back_plate"
            )
            r(
                transformer(keyboard.deco_base()), subdir, "deco_base"
            )
            r(
                transformer(
                    rotate((0, self.c.tilt - 180, 0))(rotate((-self.c.x_tilt, 0, 0))(
                        keyboard.deco_finger()
                    ))
                ), subdir, "deco_finger"
            )
            r(
                transformer(
                    rotate((0, 180, 0))(
                        left(keyboard.finger_plate.main_key_field.thumb_x_indent)(
                            keyboard.deco_thumb()
                        )
                    )
                ), subdir, "deco_thumb"
            )

        if self.c.upper_encoder_enabled:
            subdir = os.path.join("print", "extra")
            ren(
                self.rotary_knob,
                subdir,
                "encoder_knob"
            )

    def _render_test_prints(self, r):
        keyboard = self
        r(
            up(self.c.upper_encoder_socket_dimension[2]/2)(
                cube(self.c.upper_encoder_socket_dimension, center=True)
            )
            +
            down(self.c.switch_plate_thickness/2)(
                cube((20, 20, self.c.switch_plate_thickness), center=True)
            )
            -
            rotate((0, 180, 0))(
                RotaryEncoder(self.c.encoder_config)()
            ),
            "test_print",
            "finger_plate_encoder_socket"
        )

        r(
            intersection()(
                keyboard.middle(),
                keyboard.middle.promic.get_usb_front_plate_notch_marker(-18, -10)
            ),
            "test_print",
            "wall_usb_section"
        )

        r(
            rotate((180, 0, 0))(keyboard.middle.back_plate_screw_block),
            "test_print",
            "back_plate_screw_block"
        )

        r(
            intersection()(
                keyboard.deco_base(),
                down(20)(keyboard.middle.promic.get_usb_front_plate_notch_marker(-18, -50))
            ),
            "test_print",
            "base_deco_usb_section"
        )

        r(
            self.screw_pole_test_print,
            "test_print",
            "screw_pole"
        )

        r(
            keyboard.middle.test_jack_housing,
            "test_print",
            "jack_housing"
        )

        r(
            rotate((180, 0, 0))(
                down(self.c.switch_plate_thickness/2)(
                    cube((40, 28, self.c.switch_plate_thickness), center=True)
                )
                -
                self.c.switch_stamp()
            ),
            "test_print",
            "switch_mount"
        )

    def _render_visualizations(self, r):
        keyboard = self
        assembled_base = up(0)(
            color("Thistle")(keyboard.base_plate())
            +
            color("SpringGreen")(keyboard.middle())
            +
            color("RosyBrown")(keyboard.thumb_plate())
            +
            color("SandyBrown")(keyboard.finger_plate())
            +
            color("Yellow")(keyboard.deco_base())
            +
            color("Yellow")(keyboard.deco_finger())
            +
            color("Pink")(keyboard.deco_thumb())
        )

        assembled_extra = up(0)(
            color("OrangeRed")(keyboard.promic_mount_plate())
            +
            keyboard.promic_back_bar()
        )

        r(
            assembled_base + assembled_extra,
            "visualize",
            "assembled"
        )

        r(
            assembled_base,
            "visualize",
            "assembled_without_guts"
        )

        caps = (
            color("Khaki")(keyboard.thumb_caps())
            +
            color("Khaki")(keyboard.finger_caps())
        )
        if self.c.upper_encoder_enabled:
            caps += self.middle.main_key_field.upper_ecoder_nob
        r(
            assembled_base + assembled_extra
            +
            caps,
            "visualize",
            "assembled_with_caps"
        )

    def _render_dev_views(self, r):
        keyboard = self
        finger_caps = keyboard.finger_caps()
        thumb_caps = keyboard.thumb_caps()
        r(
            keyboard.deco_thumb.factor_screw_nobs([(0, 0, 0)]),
            "dev",
            "deco_screw_nob"
        )

        r(
            thumb_caps,
            "dev",
            "caps_thumb"
        )

        r(
            finger_caps,
            "dev",
            "caps_finger"
        )

        r(
            finger_caps + thumb_caps,
            "dev",
            "caps_all"
        )

        r(
            sum([
                forward(self.c.sum_key_y_width_from_top(ix))(cap())
                for ix, cap in enumerate(
                    keyboard.finger_caps.caps_for_column(column_ix=0)

                )
            ]),
            "dev",
            "finger_caps_col0"
        )

        r(
            self.c.switch_stamp(),
            "dev",
            "switch_stamp"
        )

        r(
            keyboard.deco_finger()
            +
            keyboard.deco_thumb(),
            "dev",
            "top_decos"
        )

        r(
            keyboard.deco_finger()
            +
            keyboard.deco_thumb()
            +
            keyboard.deco_base(),
            "dev",
            "decos"
        )

        r(
            color("red")(keyboard.deco_finger())
            +
            color("blue")(keyboard.finger_plate()),
            "dev",
            "finger_plate_and_deco"
        )

        r(
            color("red")(keyboard.deco_thumb())
            +
            color("blue")(keyboard.thumb_plate()),
            "dev",
            "thumb_plate_and_deco"
        )

        r(
            color("tan")(keyboard.base_plate())
            +
            color("skyblue")(keyboard.finger_plate())
            +
            color("plum")(keyboard.finger_back_plate())
            +
            color("skyblue")(keyboard.thumb_plate())
            +
            color("plum")(keyboard.thumb_back_plate())
            ,
            "dev",
            "base_and_plates"
        )

        r(
            color("skyblue")(keyboard.finger_plate())
            +
            color("plum")(keyboard.finger_back_plate())
            +
            color("yellow")(keyboard.finger_back_plate.screws)
            ,
            "dev",
            "finger_plate_w_back_plate"
        )

        r(
            color("skyblue")(keyboard.thumb_plate())
            +
            color("plum")(keyboard.thumb_back_plate())
            +
            color("yellow")(keyboard.thumb_back_plate.screws)
            ,
            "dev",
            "thumb_plate_w_back_plate"
        )

        base_with_promic = up(0)(
            color("Thistle")(keyboard.base_plate())
            +
            keyboard.promic_mount_plate()
            +
            keyboard.promic_back_bar(),
            )

        fixed_screws = color("red")(
            keyboard.base_plate.promic.back_screw
            +
            keyboard.base_plate.promic.side_screw
        )

        bottom_group = up(0)(
            base_with_promic
            +
            keyboard.middle.unwalled
            +
            fixed_screws,
            )

        r(
            bottom_group,
            "dev",
            "bottom_group"
        )

        r(
            bottom_group
            +
            keyboard.deco_base(),
            "dev",
            "bottom_group_with_deco"
        )

        r(
            base_with_promic
            +
            keyboard.middle(),
            "dev",
            "topless"
        )

        r(
            keyboard.promic_mount_plate()
            +
            color("green")(keyboard.middle() - cube((200, 200, self.c.base_plate_thickness + 0.001))),
            "dev",
            "promic_with_wall"
        )

        r(
            keyboard.promic_mount_plate()
            +
            color("green")(keyboard.deco_base()),
            "dev",
            "promic_with_base_deco"
        )

        r(
            keyboard.middle()
            +
            keyboard.middle.main_key_field.as_stamps
            +
            keyboard.middle.thumb_key_field.as_stamps
            +
            keyboard.middle.main_key_field.upper_encoder
            if self.c.upper_encoder_enabled
            else cube((0, 0, 0)),
            "dev",
            "wall_and_switches"
        )

        r(
            keyboard.finger_plate()
            +
            keyboard.finger_plate.main_key_field.as_stamps,
            "dev",
            "finger_plate_group"
        )

        r(
            base_with_promic
            +
            keyboard.middle.unwalled
            +
            forward(0)(color("Fuchsia")(keyboard.promic_mount_plate.promic_block))
            +
            keyboard.middle.main_key_field.as_stamps
            +
            keyboard.middle.thumb_key_field.as_stamps
            +
            keyboard.middle.main_key_field.upper_encoder
            if self.c.upper_encoder_enabled
            else cube((0, 0, 0)),
            "dev",
            "guts_and_parts"
        )

        r(
            base_with_promic
            +
            keyboard.middle.unwalled
            +
            forward(0)(color("Fuchsia")(keyboard.promic_mount_plate.promic_block))
            +
            keyboard.finger_back_plate()
            +
            keyboard.thumb_back_plate(),
            "dev",
            "guts_and_backplates"
        )

        r(
            base_with_promic
            +
            keyboard.middle.unwalled
            +
            keyboard.finger_plate()
            +
            keyboard.thumb_plate()
            +
            forward(0)(color("Fuchsia")(keyboard.promic_mount_plate.promic_block)),
            "dev",
            "guts_and_plates"
        )

        r(
            color("SpringGreen")(keyboard.middle())
            +
            up(0.1)(color("RosyBrown")(keyboard.thumb_plate()))
            +
            up(0.1)(color("SandyBrown")(keyboard.finger_plate())),
            "dev",
            "wall_and_tops"
        )

        r(
            keyboard.base_plate.poles(
                [ScrewPosition(0, 0, 0)],
                [10],
                translate((-5, -5, 0))(square((40, 40)))
            ),
            "dev",
            "pole_test"
        )

        r(
            keyboard.middle.promic.usb_front_plate_notch_marker_without_clearance
            +
            keyboard.middle.promic.usb_front_plate_notch_marker_with_clearance,
            "dev",
            "usb_notch_marker"
        )

        r(
            intersection()(keyboard.middle(), keyboard.middle.promic.get_usb_front_plate_notch_marker(15, 9))
            +
            forward(8)(keyboard.promic_mount_plate()),
            "dev",
            "usb_front_plate"
        )

        r(
            self.switch_position_cube_test,
            "dev",
            "swith_positions_cube_cut"
        )

        r(
            self.surface_transform_test,
            "dev",
            "surface_transform_test"
        )
