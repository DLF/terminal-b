from termb.generic.keycab import shells, interfaces, Cap
from termb.configuration import Configuration
from solid import *
from solid.utils import *
import math


def radial_base(inner_radius, outer_radius, from_angle, to_angle):
    return \
        arc(outer_radius, start_degrees=from_angle, end_degrees=to_angle, segments=400)\
        -\
        circle(r=inner_radius, segments=400)


def radial_footprint(radius, upper_length, lower_length, angle_left, angle_right, x_cutoff):
    inner_radius = radius - lower_length
    outer_radius = radius + upper_length
    from_angle = 90 - angle_right
    to_angle = 90 + angle_left
    return back(radius)(
        radial_base(inner_radius, outer_radius, from_angle, to_angle)
        -
        left(x_cutoff)(rotate((0, 0, -angle_right))(square((20, outer_radius + 20))))
        -
        right(x_cutoff)(rotate((0, 0, angle_left))(left(20)(square((20, outer_radius + 20)))))
    )


class ThumbRow1Shell(shells.MultiBase):

    @property
    def base_form(self):
        return radial_footprint(
            self.radius,
            self.upper_length,
            self.lower_length,
            self.angle_left,
            self.angle_right,
            x_cutoff=0.7
        )

    @property
    def base_length(self):
        return self.upper_length + self.lower_length

    @property
    def _slant_cylinder_length(self):
        return self.radius + self.upper_length + 10

    @property
    def _slant_cylinder_angle(self):
        return self.angle_right + self.angle_left

    @property
    def _slant_cylinder_width(self):
        return 2 * self._slant_cylinder_length * math.tan(math.radians(self._slant_cylinder_angle / 2))

    @property
    def slant_body(self):
        # return sphere(r=0.01)
        return back(self.radius)(
            scale((1, 1, 1/self.squeeze))(
                down(0.5)(
                    rotate((
                        -90 + self._slant_cylinder_angle,
                        0,
                        0
                    ))(
                        cylinder(r1=0, r2=self._slant_cylinder_width, h=self._slant_cylinder_length, segments=150)
                    )
                )
            )
        )

    def __init__(self, c:Configuration, radius, upper_length, lower_length, angle_left, angle_right):
        self.radius = radius
        self.upper_length = upper_length
        self.lower_length = lower_length
        self.angle_left = angle_left
        self.angle_right = angle_right
        self.squeeze = 2
        self.c = c
        shells.MultiBase.__init__(
            self,
            height_bottom=4,
            height_top=self.c.thumb_row_1_caps_height - 4,
            shrink_factor=self.c.thumb_row_1_caps_shrink_factor
        )


class OuterThumbRow1Shell(ThumbRow1Shell):

    def base_form(self):
        b = super().base_form
        c = b - back(self.lower_length + 5)(square((50, self.lower_length + self.upper_length + 10)))
        box = back(self.lower_length)(square((14, self.lower_length + self.upper_length)))
        return c + box


class ThumbRow1Cap(Cap):
    def __init__(self, c:Configuration, radius, upper_length, lower_length, angle_left, angle_right, interface):
        Cap.__init__(
            self,
            shell=ThumbRow1Shell(c, radius, upper_length, lower_length, angle_left, angle_right),
            interface=interface
        )


class OuterThumbRow1Cap(Cap):
    def __init__(self, radius, upper_length, lower_length, angle_left, interface):
        Cap.__init__(
            self,
            shell=OuterThumbRow1Shell(radius, upper_length, lower_length, angle_left, 1),
            interface=interface
        )


class FingerCap(Cap):
    def __init__(self, c: Configuration, elongation, tilt, extra_height, width, length, x_offset, rotation=0):
        self.c = c
        length = length + elongation
        top_height = 2 + extra_height
        bottom_height = self.c.cap_interface.height + 1 + 0.8  # 6
        Cap.__init__(
            self,
            shell=shells.Square(
                wall=1,
                indent=0.8,
                width=width,
                length=length,
                height=top_height + bottom_height,
                y_offset=-elongation/2,
                top_height=top_height,
                shrink_factor=0.75,
                squeeze=0.5,
                tilt=tilt,
                x_offset=x_offset,
                rotation=rotation
            ),
            interface=self.c.cap_interface
            # interface=interfaces.MXInterface(
            #     height=bottom_height - 1 - 0.8
            # )
        )


class ThumbRow2Shell:
    def __init__(self, c: Configuration, from_angle, to_angle):
        self.from_angle = from_angle
        self.to_angle = to_angle
        self.c = c

    @property
    def y_length(self):
        return self.c.thumb_row_2_caps_upper_length + self.c.thumb_row_2_caps_lower_length

    @property
    def x_length(self):
        return self.c.thumb_row_2_caps_base_height

    @property
    def solid_body(self):
        rounding_scale = self.c.thumb_row_2_caps_rounding_height / self.y_length
        angle = self.to_angle - self.from_angle
        return back(self.c.thumb_cluster_secondary_radius)(
            rotate(-90 - self.to_angle)(
                rotate_extrude(angle=angle, segments=400)(
                    rotate((0, 0, 90))(
                        forward(self.c.thumb_cluster_secondary_radius - self.c.thumb_row_2_caps_lower_length)(
                            square((self.x_length, self.y_length))
                            +
                            translate((self.x_length, self.y_length / 2, 0))(
                                intersection()(
                                    scale((rounding_scale, 1, 1))(circle(d=self.y_length, segments=100)),
                                    back(50)(square((100, 100)))
                                )
                            )
                        )
                    )
                )
                -
                rotate((0, -self.c.thumb_row_2_caps_slant, 90))(cube((20, 200, 20)))
                -
                rotate((0, self.c.thumb_row_2_caps_slant, 90 + angle))(right(-20)(cube((20, 200, 20))))
            )
        )

    @property
    def footprint(self):
        return projection(cut=True)(self.solid_body)

    @property
    def inner_cutout(self):
        return down(0.001)(
            linear_extrude(self.c.thumb_row_2_caps_inner_height, scale=0.9)(
                offset(r=-1)(self.footprint)
            )
        )

    @property
    def stem_base(self):
        return intersection()(
            self.solid_body,
            cube((9, 2*self.y_length, 50), center=True)
        )

    def __call__(self):
        return down(self.c.thumb_row_2_caps_inner_height)(
            self.solid_body - self.inner_cutout + self.stem_base
        )


class ThumbRow2Cap(Cap):
    def __init__(self, c: Configuration, from_angle, to_angle, interface):
        Cap.__init__(
            self,
            shell=ThumbRow2Shell(c, from_angle, to_angle),
            interface=interface
        )

