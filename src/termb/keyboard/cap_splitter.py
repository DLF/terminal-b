"""
Strategy classes for post-processing (splitting) key caps into several print parts.
"""

from solid import *
from solid.utils import *


class Simple:
    name = "simple"

    def __call__(self, cap):
        return [("simple", cap)]


class Hat:
    name = "hat"

    def __init__(
        self,
        split_z=0.8,
        connect_block_dimension=None,
        connect_block_top_clearance=0.56,
        connect_block_side_clearance=0.15,
        connect_block_rounding_radius=0.4,
        sag_depth=0.2,
        sag_wall_thickness=0.4,
        marker_depth=0.31,
        marker_radius=1
    ):
        if connect_block_dimension is None:
            # effective height is 1.0 because 0.2 get lost in the sag and 0.4 get lost for clearance
            connect_block_dimension = 5, 2, 1.6

        self.split_z = split_z
        self.connect_block_dimensions = connect_block_dimension
        self.connect_block_top_clearance = connect_block_top_clearance
        self.connect_block_side_clearance = connect_block_side_clearance
        self.connect_block_rounding_radius = connect_block_rounding_radius
        self.sag_depth = sag_depth
        self.sag_wall_thickness = sag_wall_thickness
        self.marker_depth = marker_depth
        self.marker_radius = marker_radius

    def __call__(self, cap):
        split_cube = up(self.split_z + 50)(cube((100, 100, 100), center=True))
        upper = up(0)(
            intersection()(
                split_cube,
                cap
            )
            -
            up(self.split_z + self.connect_block_dimensions[2] / 2 - 0.005)(
                cube(self.connect_block_dimensions, center=True)
            )
        )
        sag = linear_extrude(self.sag_depth + 1)(
            offset(r=-self.sag_wall_thickness)(
                projection(cut=True)(
                    down(self.split_z)(cap)
                )
            )
        )
        lower = up(0)(
            difference()(
                cap,
                split_cube,
                up(self.split_z - self.sag_depth)(
                    sag
                )
            )
            +
            up(self.split_z - self.sag_depth)(
                linear_extrude(self.connect_block_dimensions[2] - self.connect_block_top_clearance + self.sag_depth)(
                    offset(r=self.connect_block_rounding_radius, segments=40)(
                        offset(r=-self.connect_block_rounding_radius - self.connect_block_side_clearance)(
                            square(self.connect_block_dimensions[0:2], center=True)
                        )
                    )
                )
            )
            -
            up(
                self.split_z
                +
                self.connect_block_dimensions[2]
                -
                self.connect_block_top_clearance
                -
                self.marker_depth
            )(
                back(self.connect_block_dimensions[1] / 2 - self.connect_block_side_clearance)(
                    cylinder(r=self.marker_radius, h=2*self.marker_depth, segments=40)
                )
            )
        )
        return [("lower", lower), ("upper", upper)]
