import math
from solid import *
from solid.utils import *
from termb.generic.nut import Nut

class ScrewBase:
    def __init__(
            self,
            diameter,
            outer_rounding,
            angle,
            cylinder_height,
            tip_offset,
            screw_diameter,
            screw_offset,
            nut_height,
            nut_width,
            nut_z
    ):
        self.diameter = diameter
        self.outer_rounding = outer_rounding
        self.angle = angle
        self.tip_offset = tip_offset
        self.cylinder_height = cylinder_height
        self.screw_diameter = screw_diameter
        self.screw_offset = screw_offset
        self.nut_height = nut_height
        self.nut_width = nut_width
        self.nut_z = nut_z
        self._base_height = math.tan(math.radians(angle)) * diameter / 2

    def __call__(self):
        return up(0)(
            left(self.tip_offset)(
                rotate(180, (1, 0, 0))(rotate(0)(
                    linear_extrude(height=self._base_height, scale=0)(
                        right(self.tip_offset)(circle(d=self.diameter, segments=50))
                    )
                ))
            )
            +
            cylinder(h=self.cylinder_height, d=self.diameter, segments=50)
            - down(0)(  # self._base_height - 1)(
                right(self.screw_offset)(
                    color("red")(cylinder(h=self._base_height + self.cylinder_height + 2, segments=50))
                )
            )
            - up(self.nut_z)(
                right(self.screw_offset)(
                    back(self.nut_width / 2)(
                        color("yellow")(cube([self.diameter + 2, self.nut_width, self.nut_height]))
                    )
                    +
                    Nut(size=self.nut_width, height=self.nut_height)()
                )
            )
        )


def test_screwbase():
    test_screwbase = ScrewBase(
        diameter=14,
        outer_rounding=c.corner_radius,
        angle=55,
        cylinder_height=10,
        tip_offset=0,
        screw_diameter=c.screw_hole_diameter,
        screw_offset=3.5,
        nut_height=c.nut_height,
        nut_width=c.nut_width,
        nut_z=3
    )
    scad_render_to_file(test_screwbase(), "screwbase_test.scad")


def test_double_slant():
    foo = up(0)(
        rotate((10, 0, 0))(
            rotate((0, -20, 0))(
                cube((150, 100, 3))
            )
        )
    )
    scad_render_to_file(foo, "double_slant.scad")


if __name__ == "__main__":
    test_double_slant()