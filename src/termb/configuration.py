from __future__ import annotations
import math
from termb.generic.forms import RoundingStamp
from termb.generic.mx_switch import MXSwitchStamp
from termb.generic.keycab.interfaces import MXInterface
from termb.generic.rotary_encoder import RotaryEncoderConfig
from termb.generic.flattened_shaft import FlattenedShaftConfig
from termb.generic.rotary_knob import RotaryKnobConfig
from termb.keyboard import cap_splitter


class FingerCapConfiguration:
    def __init__(self, elongation: float, tilt: float, extra_height: float, rotated: bool = False):
        self.elongation = elongation
        self.tilt = tilt
        self.extra_height = extra_height
        self.rotated = rotated


class PromicroConfiguration:

    # width and length of the board with clearance
    promic_width = 18.4
    promic_length = 33.2

    # geometry of the side-bars (notch bars) and notches where the board is slit in
    notch_bar_width = 3
    notch_bar_height = 4.8
    notch_depth = 0.6
    notch_bottom_level = 1.6  # = space between ground plate and the PCB
    notch_height = 1.8  # = PCB thickness + clearance

    # depth of the front notch (where the USB connector is); notch height is used from the side notches
    front_notch_depth = 1.5

    # depth of the back notch (where the USB connector is); notch height is used from the side notches
    back_notch_depth = 0.8

    # the geometry of the USB connector cut-out
    usb_socket_width = 8.4
    usb_socket_height = 3.4
    usb_socket_depth = 2  # also smaller cut-away for the USB-plug
    usb_plug_width = 13
    usb_plug_height = 8.2
    usb_plug_cutaway_bottom_level = 1

    #
    lock_bar_vert_clearance = 0.16  # clearance at the bottom and below and above the catches
    lock_bar_hori_clearance = 0.4


class Configuration:
    # the config's name
    name = "default"

    # the configuration object for the promicro board
    promic_config = PromicroConfiguration

    # the available screw lengths (just for help output)
    available_screw_lengths = [5, 6, 8, 10, 12, 14, 16, 18, 20, 22, 25, 30]

    # the switch to use
    switch_stamp = MXSwitchStamp(
        # Some Gaterons I have seem to have a more tight fit, so that the clip-height should
        # be reduced from 1.6 to 1.4 mm.
        # clip_height=1.4,
    )()

    # ####################################################
    #   key topology and arrangement
    # ####################################################

    # How many key rows does the finger field have?
    main_field_num_rows = 3
    # Number of the first (outer) columns which have an additional key at the lower end
    main_field_extra_row_cols = 3
    # Defines the number of columns and their lowering in mm
    main_field_col_offsets = [17, 16, 4, 0, 4, 6]  # , 4]
    # Rectangular x- and y-widths for the keys on the finger plate.
    # → 19.05 for std. caps and std. spacing
    # → 18.00 is a working smaller value, for thighter switches; std. caps won’t work with such small values!
    # → see https://matt3o.com/anatomy-of-a-keyboard/
    # The x- and y-width are defined in two lists for each column or row respectively.
    # The lists may be longer than the number of columns or rows, but not shorter.
    # x-widths are given outside to inside, y-widths are given bottom to top.
    key_x_width = 19  # s = [18] * 6
    key_x_extra_gaps = [0] * 5
    key_y_widths = [18] * 4
    # Additional x-distance between the main section (finger field) and the thumb cluster.
    # (Added to the finger section.)
    thumb_cluster_x_spacing = 2.4
    # Additional y-distance between the main section (finger field) and the thumb cluster.
    # (Added to the finger section.)
    thumb_cluster_y_spacing = 0.8
    # The radius for the radial arranged thumb keys
    thumb_cluster_radius = 72
    thumb_cluster_secondary_radius = 50
    # The angle for each of the radial arranged thumb keys; also defines the number
    thumb_cluster_radial_positions = [-7 + 16 * i for i in range(4)]
    # The angles for the 2nd thumb row; Empty list if no 2nd thumb row is wanted
    thumb_cluster_secondary_radial_positions = [-7 + 16/2 + i*16*2 for i in range(2)]
    # x-distance of the radial center point from outer-most end of the thumb field
    thumb_cluster_center_x = 29.4

    # ####################################################
    #   Basic Form
    # ####################################################

    # tilt angle of main key field
    tilt = 9    # note: terminal-a had a tilt of 9.55
    x_tilt = 3
    # height of the outer edge
    outer_height = 12.6
    # height of the thumb keys (must fit to an available screw length (screw-length + screw_head_extra_indent + x))
    thumb_cluster_height = 14 + 1.4 + 0.4

    # ####################################################
    #   Screws and nuts general parameters
    # ####################################################

    screw_hole_diameter = 3.5
    screw_head_hole_diameter = 5.6 + 0.5  # DIN 963 + clearance
    nut_height = 2.4 + 0.2  # DIN 934 + clearance
    nut_width = 5.5 + 0.2  # DIN 934 + clearance
    # extra clearance for the nut holes on the bottom of the base plate.
    # Clearance is including both sides. So, a clearance of 0.2 means that each edge of the nut has 0.1
    # additional clearance to the side of the nut hole.
    bottom_nut_extra_clearance = 0.2
    # Indentation of the screws which go into the base plate from beneath (= short middle part and promic screws)
    bottom_screw_indent = 0.4

    # ####################################################
    #   Switch plate screws and their nuts and poles
    # ####################################################

    # How far are the screws away from the outside of the poles? (=distance to the walls from the middle part)
    screw_side_distance = 3.8
    # How thick is the pole’s wall towards the inside of the case?
    screw_pole_thickness = 1.4
    # How thick is the pole’s base? (That’s where the nut is)
    screw_pole_base_thickness = 4
    # The radius of the inner pole rounding
    screw_pole_rounding_radius = 3
    # The length of the little pole on the plate side
    screw_pole_counter_length = 1.6
    # Indentation of the screws which go into the key plates
    screw_head_extra_indent = 1.4

    # Depth of the nut holes in the bottom of the case
    nut_depths_main_part = [3.6, 4.2, 5, 7.6, 4.8]  # ol, ou, il, iu, m
    nut_depth_thumb_cluster = 3.4

    # Height of the thicker base part of the poles (for the nuts), including the base plate
    screw_pole_base_height_above_nut = 3

    # play above the screw poles (towards the switch plates); also used on top of the bottom connectors;
    # use it only if really necessary, it puts stress on the screw poles
    screw_pole_top_clearance = 0

    # length by with the pole forms are shrunken and back-enlarged for rounding corners; also used for thumb plate notch
    screw_pole_smoothing_length = 1.5

    # ####################################################
    #   Lengths of (some) screws
    #   (exact length only relevant for visualization)
    # ####################################################

    screw_length_promic_back_bar = 8
    screw_length_promic_side = 6
    screw_length_bottom_connect = 6
    screw_length_back_plate = 8

    # ####################################################
    #   Detail Geometry
    # ####################################################

    # border around keys; applied on the tilted surface for the main key field
    key_field_border = 2
    # cylinder rounding radius
    corner_radius = 7
    # extra border on the inner side of the main key field; applied on the tilted surface
    main_key_field_extra_inner_border = 10
    main_key_field_extra_outer_border = 1
    main_key_field_extra_lower_border = 10
    main_key_field_extra_upper_border = 0
    # rounding radius for the corner to the thumb cluster
    thumb_cluster_corner_rounding_radius = 4
    # maximum radius of the two (one convex, one concave) roundings on the outer/lower thumb cluster
    thumb_cluster_bridge_max_rounding_radius = corner_radius
    # top border of the thumb section from the switch center
    thumb_key_field_extra_top_border = 16
    # lower border of the thumb section from the switch center
    thumb_key_field_extra_lower_border = 14
    # angle where the radial part of the thumb cluster ends
    thumb_cluster_end_angle = thumb_cluster_radial_positions[-1] + 9
    # manually adjusted to avoid collision of the screw pole and the switch
    thumb_outer_box_extra_width = 2.2
    # base plate thickness
    base_plate_thickness = 2.2
    # thickness of the switch plates (thumb and finger)
    switch_plate_thickness = 3.2
    # thickness of the walls; separation wall between thumb and finger section will be double this width
    wall_width = 2.4
    # how much the switch plates are indented into the case;
    # the thickness of the switch plate brim will be switch_plate_thickness - switch_plate_notch_height;
    # the clearance towards the screw poles is defines by screw_pole_top_clearance
    switch_plate_notch_height = 1.2
    # play towards the wall from the inside
    # (affects screw poles, the notches and the front plate of the promic mount unit)
    wall_clearance = 0.2
    # play towards the wall from the outside (used by the USB plate)
    wall_outside_clearance = 0.2
    # how much the usb-connector plate excesses the wall on the outside
    usb_connector_outer_excess = 3
    # how much the usb-connector plate excesses the wall on the inside
    usb_connector_inner_excess = 1.8
    # radius of the corner rounding on the outside excess of the USB plate
    usb_connector_corner_rounding_radius = 1.5
    # depth of the notch on the sides and on top of the USB plate
    usb_connector_notch_depth = 2.6
    # clearance to the inside of the wall; the clearance to the face is determined by wall_clearance
    usb_connector_notch_side_clearance = 0.6
    # z-clearance to the upper wall
    usb_connector_notch_top_clearance = 1.2

    # cable cutout in wall between thumb and finger
    # the x-offset also affects the cutout in the thumb backplate
    cable_cutout_width = 20
    cable_cutout_height = 5
    cable_cutout_x_offset = 20

    # ####################################################
    #   Phone Jack
    # ####################################################

    # Used this jack spec
    # https://www.reichelt.de/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=C160%2FDS_KLB4.pdf
    jack_diameter_tube = 8.3
    jack_diameter_thread = 6.4
    jack_length_tube = 11.8 + 6.4
    jack_length_thread = 4.5
    jack_diameter_nut = 8
    jack_length_nut = 2
    jack_position_height = 12
    jack_wall_thickness = 4
    jack_indent = 2
    jack_housing_length = 12
    # y-clearance between jack housing and the lower-inner pole
    jack_to_pole_clearance = 0.3
    # how much of the jack is excavated on the upper part
    jack_excavation_length = 4
    # how much the excavation is lowered from the jack’s center
    jack_excavation_lowering = 1.2

    # ####################################################
    #   Bottom Connectors
    # ####################################################

    # diameter of the screw connectors on the middle part, which hold the nuts
    bottom_connector_diameter = 11
    # ... and their thickness
    bottom_connector_height = 2.8
    # the thickness of the triangular braces at the side of the bottom connectors
    bottom_connector_bracing_thickness = 1
    # x-offset from the middle where the bottom connector on the top side is placed;
    # necessary to keep feasible distance to the promic mount unit
    top_bottom_connector_x_offset = -5
    # radial position of the thumb cluster bottom connector
    thumb_cluster_bottom_connector_angle =\
        thumb_cluster_radial_positions[2]\
        - \
        (thumb_cluster_radial_positions[2] - thumb_cluster_radial_positions[1]) / 2 \
        if len(thumb_cluster_secondary_radial_positions) == 0 else \
        thumb_cluster_secondary_radial_positions[1] \
        - \
        (thumb_cluster_secondary_radial_positions[1] - thumb_cluster_secondary_radial_positions[0]) / 2

    # ####################################################
    #   Controller Mount
    # ####################################################

    promic_plate_thickness = 1
    promic_usb_plate_height = 14.2
    promic_back_bar_width = 10
    promic_back_bar_height = 4.8
    promic_back_bar_clearance = 0.2
    promic_height = 16  # only for debug purposes
    promic_indent = 15

    # ####################################################
    #   Decoration Parts
    # ####################################################

    decoration_wall_thickness = 1.2
    decoration_bottom_thickness = 1
    decoration_top_thickness = 1
    decoration_wall_clearance = 0.2
    # height of the nob part which has a conical reduction of the diameter (just to make it easier to put the nob in)
    decoration_top_screw_nob_conical_height = 0.2
    # vertical gap between the screw and the nob
    decoration_top_vertical_screw_clearance = 0.5
    # clearance around the nob; so, the radius (not diameter) of the nob is decreased by this value
    decoration_top_horizontal_screw_clearance = 0.3
    decoration_top_nob_excess = 0
    decoration_top_nob_y_offset = 1.2

    decoration_nob_plate_x_length = 10
    decoration_nob_plate_y_length = 7
    decoration_nob_plate_corner_radius = 4
    decoration_nob_x_length = 5
    decoration_nob_y_length = 2.5
    decoration_nob_corner_radius = 1.0
    decoration_nob_horizontal_clearance = 0.2

    decoration_base_outer_height = 2.4
    decoration_base_tilt = 0.5
    decoration_base_x_tilt = 0.5
    decoration_base_rim = 10
    decoration_base_usb_plate_notch_depth = 1.6
    decoration_base_usb_plate_side_clearance = 0.3
    decoration_base_usb_plate_top_clearance = 0.4
    decoration_base_usb_plate_front_clearance = 0.3

    decoration_finger_outer_height = outer_height - switch_plate_thickness - 1
    decoration_finger_tilt = tilt - 1.5
    decoration_finger_x_tilt = x_tilt - 1
    decoration_finger_rim = 2
    decoration_finger_to_thumb_vertical_cleareance = 0.2
    decoration_finger_to_thumb_horizontal_cleareance = 0.2

    decoration_thumb_height = thumb_cluster_height - switch_plate_thickness - 0.4
    decoration_thumb_rim = 2
    decoration_thumb_nob_angle = 1 * (
        thumb_cluster_secondary_radial_positions[0]
        +
        (
                thumb_cluster_secondary_radial_positions[1]
                -
                thumb_cluster_secondary_radial_positions[0]
        ) / 2
    )

    screw_cover_radius = 3.5

    # ####################################################
    #   Back plates
    # ####################################################

    back_plate_thickness = 0.6
    back_plate_spacing = 9 - switch_plate_thickness
    back_plate_minimum_bottom_spacing = 5
    back_plate_reduction = 1
    back_plate_rounding = 2

    back_plate_screw_block_width = 9
    back_plate_screw_block_base_width = 12
    back_plate_screw_block_offset = 0.2
    back_plate_screw_block_thickness = screw_head_hole_diameter + 0.4
    back_plate_screw_lift = back_plate_spacing + back_plate_thickness - screw_length_back_plate + 2.4 + 0.8
    # z-clearance for the back plate screws
    back_plate_screw_head_clearance = 0.2
    # extra side clearance for the screw's head
    back_plate_screw_head_diameter_clearance = 0.1
    # extrs side clearance for the screw
    back_plate_screw_diameter_clearance = 0.2

    # screw block adjustment
    # The upper finger block is placed over the 2nd column. This value is an offset which can
    # be necessary for example when using a rotary encoder.
    finger_plate_upper_block_x_offset = 0

    # Size of the two cable holes in the back plates;
    # All sizes are sizes of leveled cutout cubes, not of the hole on the back plate surface.
    # So for the finger plate holds: the greater the tilts, the smaller the holes for certain values.
    # The back plate reduction and rounding is applied afterwards and is added to these cut-outs.
    finger_back_plate_back_cable_cutout_pre_depth = 0
    finger_back_plate_back_cable_cutout_pre_width = 16
    finger_back_plate_front_cable_cutout_pre_depth = 7
    finger_back_plate_front_cable_cutout_pre_width = 18
    thumb_back_plate_cable_cutout_depth = 4
    thumb_back_plate_cable_cutout_width = 12

    thumb_back_plate_inner_screw_rad_offset = 2
    thumb_back_plate_outer_screw_x_offset = 2

    # ####################################################
    #   Rotary Encoder
    # ####################################################

    # The rotary encoder configuration
    encoder_config = RotaryEncoderConfig
    encoder_shaft_config = FlattenedShaftConfig
    encoder_knob_config = RotaryKnobConfig
    knob_to_shaft_base_distance = 1

    # The dimensions for the socket
    upper_encoder_socket_dimension = (
        RotaryEncoderConfig.base_dimension[0] + 3.2,
        RotaryEncoderConfig.base_dimension[1] + 1.2,
        2
    )

    # set one of the two below to None to disable the upper ecoder
    # the coordinates specify the center of the encoder
    upper_encoder_x_distance = None
    upper_ecoder_y_distance = None

    # The nob size, *only for visualization purposes* as the real nob is not
    # used in development views (to expensive for rendering)
    encoder_nob_diameter = 10
    encoder_nob_height = 18

    # ####################################################
    #   Key-Caps
    # ####################################################

    # Gap between two caps
    # For a small 18 mm key square width, this needs to be not more than 1.2 mm.
    # The gap might be further limited, dependent on the cap design.
    key_cap_gap = 1.1

    # More clearance than usual, because of printer inaccuracy.
    cap_interface = MXInterface(
        x1=1.31,
        y1=4.21,
        x2=4.21,
        y2=1.41,
        x_bar=6.8,
    )

    key_cap_splitter = [cap_splitter.Hat(), cap_splitter.Simple()]

    finger_caps = [
        FingerCapConfiguration(elongation=2, tilt=4, extra_height=0.4, rotated=True),
        FingerCapConfiguration(elongation=0, tilt=2, extra_height=0, rotated=True),
        FingerCapConfiguration(elongation=0, tilt=0, extra_height=0),
        FingerCapConfiguration(elongation=2, tilt=1.5, extra_height=0),
    ]

    thumb_row_1_caps_upper_length = 13
    thumb_row_1_caps_lower_length = 12
    thumb_row_1_1st_cap_left_angle = 9
    thumb_row_1_last_cap_right_angle = 9
    thumb_row_1_caps_height = 7.0
    thumb_row_1_caps_shrink_factor = 0.8

    thumb_row_2_caps_upper_length = 8
    thumb_row_2_caps_lower_length = 10
    thumb_row_2_caps_base_height = 3.2
    thumb_row_2_caps_inner_height = 4
    thumb_row_2_caps_rounding_height = 4.8
    thumb_row_2_caps_slant = 18
    thumb_row_2_caps_angle = (
            thumb_cluster_secondary_radial_positions[1] - thumb_cluster_secondary_radial_positions[0] - 2
        )

    def __init__(self):
        if len(self.thumb_cluster_secondary_radial_positions) == 0:
            print("Warning! No support for thumb back plate if only one thumb switch row is used! "
                  "No need to print it. :)")
        assert len(self.main_field_col_offsets) == len(self.key_x_extra_gaps) + 1, "key_x_extra_gaps must be one element shorter than main_field_col_offsets"
        assert self.main_field_num_rows + 1 == len(self.key_y_widths)
        assert self.main_field_num_rows + 1 == len(self.finger_caps)

    @property
    def upper_encoder_enabled(self):
        return self.upper_encoder_x_distance is not None and self.upper_ecoder_y_distance is not None

    @property
    def main_field_num_columns(self):
        return len(self.main_field_col_offsets)

    def cap_x_width(self, column: int):
        return self.key_x_widths[column] - self.key_cap_gap

    def cap_y_width(self, row: int):
        return self.key_x_widths[row] - self.key_cap_gap

    def key_x_center_from_outer(self, column: int) -> float:
        if column == 0:
            base = 0
            extra_gaps = 0
        else:
            base = column * self.key_x_width
            extra_gaps = sum(self.key_x_extra_gaps[:column])
        return base + extra_gaps + self.key_x_width / 2

    def sum_key_y_width_from_bottom(self, row: int):
        """
        Sum of the y-widths from the bottom until and *excluding* the given row.
        The row counting starts from 0.
        The sum for row 0 is always 0.
        """
        if row == 0:
            return 0
        return sum(self.key_y_widths[:row])

    def sum_key_y_width_from_top(self, row: int):
        """
        Sum of the y-widths from the top until and *excluding* the given row.
        The row counting starts from 0.
        The sum for row 0 is always 0.
        """
        if row == 0:
            return 0
        return sum(self.key_y_widths[-row:])

    @property
    def promic_usb_plate_thickness(self):
        return self.usb_connector_outer_excess + self.usb_connector_inner_excess + self.wall_width

    @property
    def screw_tube_diameter(self):
        return self.screw_hole_diameter + 2 * self.screw_side_distance

    def half_key_x(self, column: int):
        return self.cap_x_width(column) / 2

    def half_key_y(self, row: int):
        return self.cap_x_width(row) / 2

    @property
    def finger_switch_plate_vertical_thickness(self):
        return Configuration.switch_plate_thickness / (
            math.cos(math.radians(self.tilt)) * math.cos(math.radians(self.x_tilt))
        )

    @property
    def finger_back_plate_vertical_spacing(self):
        spacing = self.back_plate_spacing + self.back_plate_thickness
        return spacing / (math.cos(math.radians(self.tilt)) * math.cos(math.radians(self.x_tilt)))

    @property
    def decoration_finger_vertical_thickness(self):
        return Configuration.decoration_top_thickness / (
                math.cos(math.radians(self.tilt)) * math.cos(math.radians(self.x_tilt))
        )

    @property
    def corner_rounding_stamp(self):
        return RoundingStamp(self.corner_radius)

    @property
    def thumb_cluster_corner_rounding_stamp(self):
        return RoundingStamp(self.thumb_cluster_corner_rounding_radius)
