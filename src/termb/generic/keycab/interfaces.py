from solid import *
from solid.utils import *

from termb.generic.forms import RoundingStamp

class MXInterface(object):
    """
    Fits to original Cherry MX and Gateron.
    """
    def __init__(
            self,
            height=4,
            x1=1.3,
            y1=4.2,
            x2=4.2,
            y2=1.4,
            x_bar=6,
            y_bar=3.2,
            stem_circle_radius=2.75,
            bar_rounding=0.25,
            corner_rounding_diameter=0.2,
            corner_rounding_offset=0.07,
            inner_rounding_radius=0.25
        ):
        """
        :param height: The height of the pole in mm. Default is 4.
        """
        self.height = height
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.x_bar = x_bar
        self.y_bar = y_bar
        self.stem_circle_radius = stem_circle_radius
        self.bar_rounding = bar_rounding
        self.corner_rounding_diameter = corner_rounding_diameter
        self.corner_rounding_offset = corner_rounding_offset
        self.inner_rounding_radius = inner_rounding_radius

    def post(self):
        """
        Middle post width the cross adapter.
        :return: The pole.
        """

        def c(angle):
            o = self.corner_rounding_offset
            return rotate((0, 0, angle))(
                translate((o, -o, 0))(
                    circle(d=self.corner_rounding_diameter, segments=20)
                )
            )

        inner_rounding = RoundingStamp(self.inner_rounding_radius)

        cross = up(0)(
            # base cross
            translate((0, 0, self.height / 2))(
                square((self.x1, self.y1), center=True)
                +
                square((self.x2, self.y2), center=True)
            )
            # outer corner cutouts
            +
            left(self.x1 / 2)(
                forward(self.y1 / 2)(c(0))
                +
                back(self.y1 / 2)(c(90))
            )
            +
            right(self.x1 / 2)(
                forward(self.y1 / 2)(c(270))
                +
                back(self.y1 / 2)(c(180))
            )
            +
            left(self.x2 / 2)(
                forward(self.y2 / 2)(c(0))
                +
                back(self.y2 / 2)(c(90))
            )
            +
            right(self.x2 / 2)(
                forward(self.y2 / 2)(c(270))
                +
                back(self.y2 / 2)(c(180))
            )
            # inner corner roundings
            +
            translate((self.x1 / 2, self.y2 / 2, 0))(
                rotate((0, 0, 180))(inner_rounding())
            )
            +
            translate((-self.x1 / 2, self.y2 / 2, 0))(
                rotate((0, 0, -90))(inner_rounding())
            )
            +
            translate((self.x1 / 2, -self.y2 / 2, 0))(
                rotate((0, 0, 90))(inner_rounding())
            )
            +
            translate((-self.x1 / 2, -self.y2 / 2, 0))(
                rotate((0, 0, 0))(inner_rounding())
            )
        )

        return translate((0, 0, -self.height))(
            linear_extrude(self.height)(
                circle(r=self.stem_circle_radius, segments=120)
                +
                translate((0, 0, self.height/2))(
                    translate((-(self.x_bar - 2 * self.bar_rounding)/2, -(self.y_bar - 2 * self.bar_rounding)/2, 0))(
                        minkowski()(
                            square((self.x_bar - 2 * self.bar_rounding, self.y_bar - 2 * self.bar_rounding)),
                            circle(r=self.bar_rounding, segments=30)
                        )
                    )
                )
                -
                cross
            )
            -
            down(1)(linear_extrude(1.2)(
                offset(r=0.2)(cross)
            ))
        )

    @staticmethod
    def cutaway():
        base_a = 18.75
        return translate((0, 0, -10))(
            color("yellow")(
                translate((0, 0, -0.01))(
                    linear_extrude(height=10, scale=0.6)(
                        square([base_a, base_a], center=True)
                    )
                )
            )
        )


class BoxInterface(object):
    def __init__(self, height=3.2):
        self.height = height

    @staticmethod
    def cutaway():
        return MXInterface.cutaway()

    def post(self):
        """
        Box inner edge: 6mm
        Box corner radius: 1.5mm (so there remain 3mm of straight edge
        Cross y: 1.1mm (per spec)
        Cross x: 1.32 mm (per spec)
        Both cross parts are 4mm long (per spec)
        :return: The pole.
        """
        return down(self.height)(
            linear_extrude(height=self.height)(
                difference()(
                    translate([-3, -3])(
                        minkowski()(
                            square([3, 3]),
                            translate([1.5, 1.5])(
                                circle(r=1.5, segments=50)
                            )
                        )
                    ),
                    square([1.2, 4], center=True),
                    square([4, 1.4], center=True)
                )
            )
        )


if __name__ == "__main__":
    i = BoxInterface()
    scad_render_to_file(i.post(), "test.scad")