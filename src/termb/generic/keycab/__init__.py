import solid
from termb.generic.keycab import interfaces, shells


class Cap(object):
    def __init__(self, shell, interface):
        self.shell = shell
        self.interface = interface

    def __call__(self, *args, **kwargs):
        return self.shell() - self.interface.cutaway() + self.interface.post()


if __name__ == "__main__":
    # solid.scad_render_to_file(interfaces.MXInterface.cutaway(), "test.scad")
    shell = shells.Square()
    interface = interfaces.MXInterface(height=5)
    solid.scad_render_to_file(
        shell.shell() - interface.cutaway() + interface.post(),
        "test.scad"
    )
