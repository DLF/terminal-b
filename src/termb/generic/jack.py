from solid import *
from solid.utils import *


class TubeJack:
    def __init__(
        self,
        diameter_tube,
        diameter_thread,
        diameter_nut,
        length_tube,
        length_thread,
        length_nut,
        color_tube=None,
        color_thread=None,
        color_nut=None
    ):
        self.diameter_tube = diameter_tube
        self.length_tube = length_tube
        self.color_tube = "red" if color_tube is None else color_tube
        self.diameter_thread = diameter_thread
        self.length_thread = length_thread
        self.color_thread = "yellow" if color_thread is None else color_thread
        self.diameter_nut = diameter_nut
        self.length_nut = length_nut
        self.color_nut = "green" if color_nut is None else color_nut
        self.segments = 60

    def __call__(self):
        return rotate((0, 90, 0))(
            color(self.color_tube)(
                cylinder(d=self.diameter_tube, h=self.length_tube, segments=self.segments)
            )
            +
            color(self.color_thread)(
                down(self.length_thread)(
                    cylinder(d=self.diameter_thread, h=self.length_thread + 0.1, segments=self.segments)
                )
            )
            +
            color(self.color_nut)(
                down(self.length_thread + 0.01)(
                    cylinder(d=self.diameter_nut, h=self.length_nut, segments=self.segments)
                )
            )
        )


class PhoneJack35(TubeJack):
    def __init__(self):
        TubeJack.__init__(
            self,
            diameter_tube=8.8,
            diameter_thread=6.4,
            length_tube=11.8 + 6.4,
            length_thread=4.5,
            diameter_nut=8,
            length_nut=2
        )


if __name__ == "__main__":
    scad_render_to_file(PhoneJack35()(), "phone_jack_35.scad")
