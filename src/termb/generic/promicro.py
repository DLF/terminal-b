from solid import *
from solid.utils import *

# Pro-Micro dimensions: 18.5mm×33.5mm ground plate (USB socket juts out by 1.9mm)
# CB is 1.5 mm thick. We can use only 0.5mm for a notch.
# The PCB is mounted 1mm above the ground plate.

# USB-socket dimensions:
# 8mm width
# 3mm height
# mounted directly on the PCB without gap

# z=0 is the surface of the base where this mount is placed on
# x/y = 0/0 is the center of the USB socket front

# the Pro Micro is lying on the x/y plane and the USB-socket-to-end axis is the x-axis.


class NotchBar:
    def __init__(self, c):
        self.c = c

    def __call__(self):
        return up(0)(
            cube([
                self.c.promic_length
                -
                self.c.front_notch_depth
                -
                self.c.back_notch_depth,
                self.c.notch_bar_width,
                self.c.notch_bar_height
            ])
            -
            color("yellow")(
                translate((-0.5, -1, self.c.notch_bottom_level))(
                    cube([
                        self.c.promic_length
                        +
                        1,
                        self.c.notch_depth + 1,
                        self.c.notch_height
                    ])
                )
            )
        )


class FrontNotch:
    def __init__(self, c):
        self.c = c

    def __call__(self):
        return color("orange")(
            translate((-self.c.front_notch_depth, -self.c.promic_width / 2, self.c.notch_bottom_level))(
                cube([self.c.front_notch_depth + 1, self.c.promic_width, self.c.notch_height])
            )
        )


class USBCutAway:
    def __init__(self, c):
        self.c = c

    def __call__(self):
        return up(0)(
            color("red")(
                translate((
                    -self.c.usb_socket_depth - self.c.front_notch_depth - 1,
                    -self.c.usb_socket_width / 2,
                    self.c.notch_bottom_level + self.c.notch_height - 0.4
                ))(
                    cube([
                        self.c.usb_socket_depth + self.c.front_notch_depth + 2,
                        self.c.usb_socket_width,
                        self.c.usb_socket_height
                    ])
                )
            ) + color("yellow")(
                translate((
                    -14 - self.c.usb_socket_depth - self.c.front_notch_depth,
                    -self.c.usb_plug_width / 2,
                    self.c.usb_plug_cutaway_bottom_level
                ))(
                    cube([15, self.c.usb_plug_width, self.c.usb_plug_height])
                )
            )
        )


class PromicBackBarCutaway:
    def __init__(self, c):
        self.c = c

    def __call__(self):
        pin_cutaway = 2.5
        return translate(
            (self.c.promic_length - self.c.front_notch_depth, self.c.notch_depth - self.c.promic_width/2, 0)
        )(
            translate((-1, -self.c.notch_depth, self.c.notch_bottom_level))(
                color("pink")(
                    cube([self.c.notch_depth + 0.24 + 1, self.c.promic_width, self.c.notch_height])
                )
            )
            +
            translate((-1, -self.c.notch_depth, 0))(
                color("red")(
                    forward(self.c.notch_depth)(
                        cube([self.c.notch_depth + 0.24 + 1, pin_cutaway, 10])
                    )
                    +
                    forward(self.c.promic_width - self.c.notch_depth - pin_cutaway)(
                        cube([self.c.notch_depth + 0.24 + 1, pin_cutaway, 10])
                    )
                )
            )
        )


class PromicFrontCutaway:
    def __init__(self, c):
        self.c = c

    def __call__(self):
        return FrontNotch(self.c)() + USBCutAway(self.c)()


class PromicMount:
    def __init__(self, c):
        self.c = c

    def __call__(self):
        n = NotchBar(self.c)
        return \
            translate((0, self.c.promic_width/2 - self.c.notch_depth, 0))(
                n()
            ) \
            + \
            translate((0, -self.c.promic_width/2 + self.c.notch_depth, 0))(
                mirror((0, 1, 0))(n())
            )
