from solid import *
from math import sqrt


# // m2
# nut_size=4;         // DIN 934
# nut_height=1.6;     // DIN 934
# // m3
# nut_size=5.5 + 0.2;        // DIN 934
# nut_height=2.4 + 0.2;      // DIN 934


def _hexagon(d):
    ra = d/2 * 2/sqrt(3)
    return circle(r=ra, segments=6)


class Nut:
    def __init__(self, size, height):
        self.size = size
        self.height = height

    def __call__(self):
        return linear_extrude(self.height)(
            _hexagon(self.size)
        )


if __name__ == "__main__":
    scad_render_to_file(Nut(10, 3)(),"nut-test.scad")
