import math

from solid import *
from solid.utils import *


class Dovetail:
    def __init__(
        self,
        top_width,
        bottom_width,
        depth,
        top_rounding,
        bottom_rounding
    ):
        assert bottom_width > top_width
        self.top_width = top_width
        self.bottom_width = bottom_width
        self.depth = depth
        self.top_rounding = top_rounding
        self.bottom_rounding = bottom_rounding

    @property
    def shape(self):
        a = self.top_width / 2
        b = self.bottom_width / 2
        d = self.depth

        f = self.f + a
        x = self.x
        y = self.y + a
        return up(0)(
            offset(r=self.bottom_rounding, segments=50)(
                offset(r=-self.bottom_rounding)(
                    polygon([
                        (0, a), (d, b), (d, -b), (0, -a)
                    ])
                )
            )
            +
            polygon([
                (0, f), (x, y), (x, -y), (0, -f)
            ])
            -
            self.cutaway_circles
        )

    @property
    def side_angle(self):
        """
        Angle of a single side in radians.
        """
        e = (self.bottom_width - self.top_width) / 2
        return math.atan(e / self.depth)

    @property
    def f(self):
        """
        The length of the rounded extra, from the end of the top-width to the rounding-spike.
        """
        alpha = (math.pi/2 - self.side_angle) / 2
        f = self.top_rounding / math.tan(alpha)
        return f

    @property
    def x(self):
        """
        The x-coordinate where the rounding circle hits the triangle.
        """
        return math.cos(self.side_angle) * self.f

    @property
    def y(self):
        """
        The x-coordinate where the rounding circle hits the triangle.
        """
        return math.sin(self.side_angle) * self.f

    @property
    def cutaway_circles(self):
        center1 = (self.top_rounding, self.f + self.top_width / 2, 0)
        center2 = (self.top_rounding, -self.f - self.top_width / 2, 0)
        return up(0)(
            color("red")(translate(center1)(circle(r=self.top_rounding, segments=100)))
            +
            color("red")(translate(center2)(circle(r=self.top_rounding, segments=100)))
        )


if __name__ == "__main__":
    dt = Dovetail(
        top_width=4,
        bottom_width=6,
        depth=4,
        top_rounding=0.6,
        bottom_rounding=0.4
    )
    scad_render_to_file(dt.shape, "shape.scad")
