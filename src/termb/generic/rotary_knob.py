from solid import *
from solid.utils import *

from termb.generic.forms import RoundingStamp


class RotaryKnobConfig:
    lower_diameter = 16
    lower_height = 15
    upper_diameter = 19
    upper_height = 8
    rounding_radius = 2
    segments_knob = 96
    segments_rounding = 80
    ripple_diameter = 1.7
    ripple_strech_factor = 0.5
    segments_ripples = 20
    ripple_angle = 15


class TerminalARotaryKnobConfig:
    lower_diameter = 24
    lower_height = 8
    upper_diameter = 32.5
    upper_height = 9
    rounding_radius = 2
    segments_knob = 96
    segments_rounding = 80
    ripple_diameter = 3
    ripple_strech_factor = 0.3
    segments_ripples = 20
    ripple_angle = 15


class RotaryKnob:

    def _ripple_cut_away(self, height):
        s = self.c.ripple_strech_factor
        d = self.c.ripple_diameter

        return scale((1, s, 1))(
            back(0.5 * d)(
                cylinder(d=d, h=height, segments=self.c.segments_ripples)
                +
                left(2 * d)(
                    cube((4 * d, d, height))
                )
                -
                sum([
                    left(d*i)(
                        down(1)(
                            cylinder(d=d, h=height+2, segments=self.c.segments_ripples)
                        )
                    )
                    +
                    left(2*d*i)(
                        up(0.5*height)(
                            cube((2*d, d, height+2), center=True)
                        )
                    )
                    for i in [-1, 1]
                ])
            )
        )

    def __init__(self, rotary_knob_config: RotaryKnobConfig):
        self.rotary_knob_config = rotary_knob_config
        assert self.c.upper_diameter > self.c.lower_diameter, \
            "The upper diameter must be greater than the lower diameter."
        assert (self.c.upper_diameter - self.c.lower_diameter) / 2 - self.c.rounding_radius <= self.c.lower_height, \
            "The lower height must be equal to or greater than the difference of the upper " \
            "and the lower radius minus the rounding radius."
        assert 360 > self.c.ripple_angle >= 0,\
            "The ripple angle must be in [0..360[."
        assert 360 % self.c.ripple_angle == 0, \
            "The ripple angle must be a divider of 360."

    @property
    def c(self):
        return self.rotary_knob_config

    def __call__(self):
        c = self.c
        return up(0)(
            rotate_extrude(segments=c.segments_knob)(
                self._rotation_form()
            )
            -
            sum([
                rotate(a)(
                    translate((0, c.upper_diameter / 2, c.lower_height - 2))(
                        self._ripple_cut_away(c.upper_height + 4)
                    )
                )
                for a in range(0, 359, c.ripple_angle)
            ])
        )

    def _rotation_form(self):
        c = self.c
        delta = (c.upper_diameter - c.lower_diameter) / 2 - c.rounding_radius
        transition_rounding_stamp = RoundingStamp(delta, segments=c.segments_rounding)
        smoothing_rounding_stamp = RoundingStamp(c.rounding_radius, segments=c.segments_rounding)
        return (
            # lower part
            square((c.lower_diameter / 2, c.lower_height))
            +
            # upper part
            forward(c.lower_height)(
                square((c.upper_diameter / 2, c.upper_height))
            )
            +
            # smoothing between lower and upper
            translate((c.lower_diameter/2, c.lower_height, 0))(
                mirror((1, 0, 0))(
                    transition_rounding_stamp()
                )
            )
            -
            # smooting uppert edge
            translate((
                c.upper_diameter/2 + 0.001,
                c.lower_height + c.upper_height + 0.001,
                0
            ))(
                smoothing_rounding_stamp()
            )
            -
            # smoothing lower edge
            translate((
                c.upper_diameter / 2 + 0.001,
                c.lower_height - 0.001,
                0
            ))(
                rotate((0, 0, -90))(
                    smoothing_rounding_stamp()
                )
            )
        )


if __name__ == "__main__":
    from termb.generic.rotary_encoder import RotaryEncoder, RotaryEncoderConfig
    config = RotaryKnobConfig()
    knob = RotaryKnob(config)

    scad_render_to_file(knob(), "knob.scad")
