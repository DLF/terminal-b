from solid import *
from solid.utils import *


class RotaryEncoderConfig:
    base_dimension = (14.2, 12.2, 12)
    shaft_length = 20
    shaft_base_length = 8
    shaft_diameter = 6
    shaft_base_diameter = 7.8
    flange_dimension = (1.6, 4.2, 5)
    flange_x_offset = 0.5
    flange_z_offset = 3
    segments = 50


class RotaryEncoder:

    def __init__(self, configuration: RotaryEncoderConfig):
        self.c = configuration

    def __call__(self):
        c = self.c
        c_inner = color("LawnGreen")(
            down(0.1)(
                cylinder(d=c.shaft_diameter, h=c.shaft_length + 0.1, segments=c.segments)
            )
        )
        c_outer = color("MediumOrchid")(
            down(0.1)(
                cylinder(d=c.shaft_base_diameter, h=c.shaft_base_length + 0.1, segments=c.segments)
            )
        )
        base = color("DarkSalmon")(
            down(c.base_dimension[2]/2)(
                cube(c.base_dimension, center=True)
            )
        )
        flange = color("MediumVioletRed")(
            back(c.flange_dimension[1]/2)(
                left(c.base_dimension[0]/2 + c.flange_x_offset)(
                    down(c.flange_z_offset)(
                        cube(c.flange_dimension)
                    )
                )
            )
        )
        return c_inner + c_outer + base + flange


if __name__ == '__main__':
    r = RotaryEncoder(RotaryEncoderConfig())
    scad_render_to_file(r(), "rotary_encoder.scad")