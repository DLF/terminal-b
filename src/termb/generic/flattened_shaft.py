from solid import *
from solid.utils import *


class FlattenedShaftConfig:
    """
    Based on ALPS EC11 Series
    """
    ground_diameter = 7 + 2
    base_diameter = 7 + 1
    shaft_diameter = 6 + 0.4
    shaft_flattened_diameter = 4.5 + 0.2
    ground_height = 2
    base_height = 1
    shaft_length = 14.5 - 2 - 1
    flattened_pads_heights = 0.4
    flattened_pads_positions = [4.8, 8.4, 12]
    flattened_pads_exposure = 0.2
    flattened_area_start_height = 4
    segments = 60


class FlattenedShaftCutaway:

    def __init__(self, config: FlattenedShaftConfig):
        self.config = config

    @property
    def c(self):
        return self.config

    def __call__(self):
        c = self.c
        flattener = left(c.shaft_diameter)(
            back(c.shaft_diameter * 0.5 - c.shaft_flattened_diameter)(
                cube((2*c.shaft_diameter, 2*c.shaft_diameter, c.flattened_pads_heights))
            )
        )
        return up(0)(
            cylinder(d=c.ground_diameter, h=c.ground_height, segments=c.segments)
            +
            up(c.ground_height)(
                cylinder(d=c.base_diameter, h=c.base_height, segments=c.segments)
            )
            +
            up(c.ground_height + c.base_height)(
                cylinder(d=c.shaft_diameter, h=c.shaft_length, segments=c.segments)
            )
            -
            color("red")(sum([
                up(h + c.ground_height + c.base_height)(flattener)
                for h in c.flattened_pads_positions
            ]))
            -
            color("yellow")(
                up(c.ground_height + c.base_height + c.flattened_area_start_height)(
                    left(c.shaft_diameter)(
                        back(c.shaft_diameter * 0.5 - c.shaft_flattened_diameter - c.flattened_pads_exposure)(
                            cube((
                                2*c.shaft_diameter,
                                2*c.shaft_diameter,
                                c.shaft_length
                            ))
                        )
                    )
                )
            )
        )


if __name__ == "__main__":
    config = FlattenedShaftConfig()
    shaft = FlattenedShaftCutaway(config)

    scad_render_to_file(shaft(), "flattened_shaft.scad")
