from solid import *


class RoundingStamp:
    def __init__(self, radius, segments=100):
        self.radius = radius
        self.segments = segments

    def __call__(self, angle=0):
        return rotate(angle)(
            translate((-self.radius, -self.radius, 0))(
                square([self.radius, self.radius]) - circle(self.radius, segments=self.segments)
            )
        )

