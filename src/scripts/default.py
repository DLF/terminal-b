from termb.keyboard import TerminalB
from termb.configuration import Configuration, FingerCapConfiguration
from termb.generic.keycab.interfaces import MXInterface
from termb.keyboard import cap_splitter

# findings for 0.2.x
# todo: mx-cross clearance: re-fit to next switch

# Minor insufficiencies / Low-value features
# todo: check thumb cluster y-width in the kernel if it still works fine for a single row thumb cluster
# todo: MainKeyField.shadow_x/y_width is not correct. Instead of the simplified 1D-rotation,
#       I should use a proper 2-axis rotation for this calculation
# todo: MainKeyField.lowest_screw_head_point could calculate the exact lowest point of a screw head rim
# todo: Configuration*vertical_thickness methods do not calculate the thickness correctly (but sufficiently)


class ThreeRows(Configuration):
    name = "3r"


class TerminalC(Configuration):
    name = "term-c"
    key_x_extra_gaps = [0, 2, 0, 2, 0]
    # Thumb cluster geometry
    thumb_cluster_radius = 67
    thumb_cluster_secondary_radius = 45
    thumb_cluster_radial_positions = [-7 + 17 * i for i in range(4)]
    thumb_cluster_secondary_radial_positions = [-7 + 17/2 + i*17*2 for i in range(2)]

    # ####################################################
    #   Key-Caps
    # ####################################################

    # Gap between two caps
    # For a small 18 mm key square width, this needs to be not more than 1.2 mm.
    # The gap might be further limited, dependent on the cap design.
    key_cap_gap = 1.1

    # More clearance than usual, because of printer inaccuracy.
    cap_interface = MXInterface(
        x1=1.31,
        y1=4.21,
        x2=4.21,
        y2=1.41,
        x_bar=6.8,
    )

    key_cap_splitter = [cap_splitter.Hat(), cap_splitter.Simple()]

    finger_caps = [
        FingerCapConfiguration(elongation=0, tilt=0, extra_height=0),
        FingerCapConfiguration(elongation=0, tilt=0, extra_height=0),
        FingerCapConfiguration(elongation=0, tilt=0, extra_height=0),
        FingerCapConfiguration(elongation=0, tilt=0, extra_height=0),
    ]

    thumb_row_1_caps_upper_length = 13
    thumb_row_1_caps_lower_length = 12
    thumb_row_1_1st_cap_left_angle = 9
    thumb_row_1_last_cap_right_angle = 9
    thumb_row_1_caps_height = 6.6
    thumb_row_1_caps_shrink_factor = 0.8

    thumb_row_2_caps_upper_length = 8
    thumb_row_2_caps_lower_length = 10
    thumb_row_2_caps_base_height = 3.2
    thumb_row_2_caps_inner_height = 4
    thumb_row_2_caps_rounding_height = 4.8
    thumb_row_2_caps_slant = 18
    thumb_row_2_caps_angle = (
            thumb_cluster_secondary_radial_positions[1] - thumb_cluster_secondary_radial_positions[0] - 2
    )



class FourRows(Configuration):
    name = "4r"

    # Use four rows
    main_field_num_rows = 4
    key_y_widths = [17.4] * 5
    key_x_extra_gaps = [0, 2, 0, 2, 0]
    # As the y-distance between the jack and the promic is larger due to the 4th switch row,
    # we can lower the jack by 2 mm. It just looks nicer.
    jack_position_height = 10
    
    # We *can* keep the outer height (= base height) a bit lower since the x-tilt gives us some more z-space
    # in the inside with the 4th row.
    # outer_height = 11.6
    # OR, we can reduce the tilt
    # (which could also be done when increasing the extra inner border to get the controller
    # further away from the swithces)
    tilt = 8.2

    # Depth of nut holes of the finger section
    nut_depths_main_part = [3.6, 5, 4, 7.4, 4.4]  # ol, ou, il, iu, m

    # Caps for four rows
    finger_caps = [
        FingerCapConfiguration(elongation=2, tilt=4, extra_height=0.4, rotated=True),
        FingerCapConfiguration(elongation=0, tilt=2, extra_height=0, rotated=True),
        FingerCapConfiguration(elongation=0, tilt=0, extra_height=0),
        FingerCapConfiguration(elongation=0, tilt=1.5, extra_height=0),
        FingerCapConfiguration(elongation=2, tilt=8, extra_height=0.6),
    ]


class FourRowsWithEncoder(FourRows):
    name = "4rEnc"

    # Depth of nut holes of the finger section
    nut_depths_main_part = [3.6, 3, 4, 7.6, 4.4]  # ol, ou, il, iu, m

    # set the encoder position (which also the enables the encoder)
    upper_encoder_x_distance = 1 * (
        FourRows.main_key_field_extra_outer_border
        + FourRows.key_field_border
        + FourRows.key_x_width + FourRows.key_x_extra_gaps[0] / 2
        - 1.4
    )
    upper_ecoder_y_distance = 1 * (
        FourRows.wall_width  # wall width assumed to be >= decoration rim
        + FourRows.upper_encoder_socket_dimension[1]/2
        + 1.0
    )
 
    # the upper border must be wider for the encoder to not conflict with the nearest key
    main_key_field_extra_upper_border = 4

    # the back plate screw block needs to be moved the outer most position to make plate for the encoder
    finger_plate_upper_block_x_offset = 3.5

    # # use caps with normal y-width also for upper row to give the encoder enough space
    # finger_caps = [
    #     FingerCapConfiguration(elongation=2, tilt=4, extra_height=0.4, rotated=True),
    #     FingerCapConfiguration(elongation=0, tilt=2, extra_height=0, rotated=True),
    #     FingerCapConfiguration(elongation=0, tilt=0, extra_height=0),
    #     FingerCapConfiguration(elongation=0, tilt=1.5, extra_height=0),
    #     FingerCapConfiguration(elongation=0, tilt=8, extra_height=0.6),
    # ]


class Beta(Configuration):
    """
    A 3-row/6-column configuration with a high tilt and high caps,
    and which can use standard keycaps.

    This configuration corresponds to the first build.
    It's not exactly the first build, due to some corrections.
    """
    name = "beta"
    # How many key rows does the finger field have?
    main_field_num_rows = 3
    # Defines the number of columns and their lowering in mm
    main_field_col_offsets = [16, 12, 4, 0, 4, 6]
    # Standard key distance, so standard caps can be used.
    key_x_width = 19.05  # s = [18] * 6
    key_y_widths = [19.05] * 4
    # Slightly bigger gap between the caps than usual
    key_cap_gap = 1.4
    # High tilt
    tilt = 12
    x_tilt = 4
    # height of the outer edge
    outer_height = 10.2

    # disable the upper encoder
    upper_encoder_x_distance = None

    # When using only 3 finger key rows, the controller plate needs to be further shifted to the outside.
    # Otherwise it collides with the jack and its cables.
    promic_indent = 21

    # Also the cabe hole in the wall is shifted a bit more to the outside
    cable_cutout_x_offset = 15

    # Higher thumb cluster (for 16 mm screws; fit of screw length is not too great...)
    thumb_cluster_height = 18.8
    nut_depth_thumb_cluster = 4

    # Finger border geometry
    key_field_border = 4
    main_key_field_extra_inner_border = 7.6
    main_key_field_extra_outer_border = 6.2
    main_key_field_extra_lower_border = 3.6
    main_key_field_extra_upper_border = 0
    thumb_cluster_x_spacing = 7
    thumb_cluster_y_spacing = 0
    # top border of the thumb section
    thumb_key_field_extra_top_border = 16 + 8 + 4
    # lower border of the thumb section
    thumb_key_field_extra_lower_border = 14 + 8 + 4

    # Depth of nut holes of the finger section
    nut_depths_main_part = [3.6, 3.8, 10.2, 14.2, 4.6]  # ol, ou, il, iu, m

    # As controller is moved further to the outside (away from the jack),
    # we can lower the jack by 2 mm. It just looks nicer.
    jack_position_height = 10

    # Thumb cluster geometry
    thumb_cluster_radius = 62
    thumb_cluster_secondary_radius = 40
    thumb_cluster_radial_positions = [-5.5 + 18 * i for i in range(4)]
    thumb_cluster_secondary_radial_positions = [-5.5 + 9, -5.5 + 9 + 2*18]
    thumb_key_field_extra_top_border = 3

    # Decoration
    decoration_base_tilt = 0.6
    decoration_base_x_tilt = 1.5
    decoration_finger_tilt = tilt - 2
    decoration_finger_x_tilt = x_tilt - 1
    decoration_finger_outer_height = outer_height - Configuration.switch_plate_thickness - 1
    decoration_thumb_height = thumb_cluster_height - Configuration.switch_plate_thickness - 1.8

    # The caps
    finger_caps = [
        FingerCapConfiguration(elongation=2, tilt=5, extra_height=0.4, rotated=True),
        FingerCapConfiguration(elongation=0, tilt=3, extra_height=0, rotated=True),
        FingerCapConfiguration(elongation=0, tilt=0, extra_height=0),
        FingerCapConfiguration(elongation=2, tilt=10, extra_height=0.6),
    ]
    thumb_row_2_caps_angle = (
            thumb_cluster_secondary_radial_positions[1] - thumb_cluster_secondary_radial_positions[0] - 2
    )


#TerminalB(ThreeRows()).render()
#TerminalB(FourRows()).render()
#TerminalB(FourRowsWithEncoder()).render()
TerminalB(TerminalC()).render()
#TerminalB(Beta()).render()
