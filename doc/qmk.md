The [QMK](https://qmk.fm/) based firmware is developed in my
[QMK-fork](https://github.com/DLFW/qmk_firmware) in the 
[terminal_b branch](https://github.com/DLFW/qmk_firmware/tree/terminal_b).

QMK is [well documented](https://docs.qmk.fm/#/). This page is just a summary for a quick start.

Be aware that my keymap is ISO-DE based and contains some choices which are quite specific for my needs.
You may want to create your own keymap.

You can clone my repo and add a keymap, or you can get a fresh QMK clone and use mine just as an inspiration.

Be also aware that the LED and color management is completely handled in my `keymap.c`.

# Setup
To build and flash the firmware,
prepare your sources (e.g. clone my repo and checkout the terminal_b branch)
and follow the
[QMK setup guide](https://beta.docs.qmk.fm/tutorial/newbs_getting_started)
to get everything needed in place.

On Arch Linux, installing `qmk-git` from the AUR should be more than enough.
I think I actually installed only these additional packages:

* avr-binutils
* avr-gcc
* avr-libc
* avrdude

I furthermore added a UDEV rule to be able to flash the board without root privileges.
When using the QMK-CLI tool (`qmk setup`), as descibed on the QMK setup tutorial, the UDEV rule is created automatically.
When no UDEV rule exist, one can only flash the firmware with `sudo`.
For just flashing it once in a while, that might be fine.
But when you change the code a lot, it can be annoying when there are files compiled with sudo which remain under root
ownership in the repo.

# Variants
Terminal B can be build with different key layouts. Layouts differ in nuber of keys and/or key-arrangement. 
Each layout needs a different firmware variant.

Like all QMK boards with multiple variant, the are specified as `<board>/<variant>` in a make target.

Terminal B variants follow the naming scheme `NxN_NNN`, witch each N being a number.
The first two numbers specify the columns and rows of the main key section, the third the number of
keys in row 0. The fourth and fifth numbers specify the number of keys in the first and sections thumb row.

# Compiling
To compile the firmware from the repository root directory, call
```
make terminal_b/<variant>:<keymap>
```
For example:
```
make terminal_b/6x3_342:default
```
`default` is the keymap. You can change it to another keymap, of course.

# Flashing
To flash each of the two board sites, just run
```
make terminal_b/<variant>:<keymap>:avrdude
```
from the repository root and then reset the controller.

If the firmware is already flashed, you can use the reset key on the META layer.
For a virgin board or if the keymap has been changed in a way that no software reset key is available,
connect and disconned the reset pin on the promicro board with GND to reset the board.

Terminal B uses
“[handedness by EEPROM](https://docs.qmk.fm/#/feature_split_keyboard?id=handedness-by-eeprom)”,
so the information if the board is the left or the right needs to be written to the EEPROM once.
To do so, call each of these make targets once for their respective side of the board:
```
make terminal_b/<variant>:<keymap>:avrdude-split-left
make terminal_b/<variant>:<keymap>:avrdude-split-right
```

# Notes
* Terminal B uses the ProMicro board, which comes with the *Caterina* bootloader.
* Notes on the Caterina bootloader and the respective make targets can be found at https://docs.qmk.fm/#/flashing?id=caterina.
* Compiling and flashing might also work with the qmk tool, but I just directly call `make`. However, it should work with commands like these:
  ```
  qmk compile -kb terminal_b -km default
  qmk flash -kb terminal_b -km default
  ```

