# 3D-Printing the Keyboard

The board does not have any special requirements to the printer.
I use a stock Ender 3.
A resin printer should also work for the board, but there might be a problem with a missing drainage for the caps.

I used PETG, but PLA should just work fine.
PETG might just be better for the durability of the keyboard and also causes less trouble with warping during printing for me.

All case parts can be sliced with mostly the same parameters.

In any case, you want to avoid warping. Warping would make most parts unusable.

## Keyboard Case

### Layer Height = 0.2 mm
All parts should be printed with 0.2 mm layer height (or a factor of 0.2, like 0.1 or 0.05).
This is because all significant z-coordinates of the model have been aligned to a 0.2 multiple.

### Line Width = 0.4 mm
The line width should be larger than the layer height, of course.
The walls and some other xy-structure have a thickness of 1.2 mm or multiples of it.
Using a line width of 0.3 or 0.4 mm will reduce the need for fillings during the print and increases both,
print speed and stability, since they are both integral dividers of 1.2 mm.
As 0.4 mm work perfectly fine and are faster to print, it’ the recommended line width.

### Wall Thickness >= 2.4 mm (10 lines recommended)
The wall thickness should be at least 2.4 mm.
This means at least 6 wall lines for a line width of 0.4 mm or at least 8 wall lines when using a line width of 0.3 mm.
This brings stability to the plates and a more equally flat surface on the outside of the base plate where the wall sits on.

Using 10 wall lines (wall width = 4 mm with a line width of 0.4mm) works perfectly fine and makes printing of some parts’
first layer easier.

### Infill and Top & Bottom Layers
A simple, fast printing 50 % infill should be fine. I use 3 top and 3 bottom layers.
I would *not* recommend to use less, but 3 are sufficient.
Of course, increasing the number of top and bottom layers or the infill will increase the stability.

If printing the switch plates transparent for illumination, the fill pattern should be a 3D fill pattern
to have a more equal dispersion of light.

### Speed: Low
Most parts have some very fine structures that need to be printed pretty precisely,
like the holes for the nuts on the bottom.
Corners must be precise and blobs and other print artifact need to be minimized.
A low print speed is one important (but of course not sufficient) condition to keep the print clean.

I print the outer wall lines with 15 mm/s, the inner walls and the top and bottoms with 25 mm/s,
and the initial layer limited to 20 mm/s. I print the infill with 40 mm/s.

### Seam: Away from Corners
Yes, you should try to keep “seam points” away from corners.
While this is often done on purpose to hide the seam, it comes with a heavy downside for technical models:
it decreases the adhesion quality of the first layer exactly in a position where adhesion is most important.
Therewith, it increases the chance of warping in the chosen corner.

Best is to place the seam points on a straight part of a wall (inner wall if it exists) where the respective
part does not touch another part.
But even random placement of the seam-point is better than putting it at a corner.

This is of importance for the “wall” part and the “thumb plate”, but it doesn’t hurt the others.

### Support: None
Support is *not* needed at all.

There are only a few overhangs:
Three in the wall-part (the jack-slot, the USB-cutout and the cable hole), and the USB-connector-cutout in the controller plate,
but they usually don't need support.
There are some additional, very tiny overhangs in the controller plate and the controller back-bar
(over the slots where the circuit board is slid in), but they also can be printed without support and the structures are anyway too
small to properly use and remove support.

### Adhesion Support: None (but...)
Adhesion support is not necessary.
However, the “middle” part might be easier to print successfully with some brim.
Take care that the brim can be easily removed, as it will come in your way later otherwise.

### Notes on Individual Parts
* Middle (Walls): Small brim for adhesion support can be helpful but must be carefully removed afterwards.
* Finger and Thumb Plate: With 100% infill and a 3D fill pattern, in case it’s transparent and illuminated.
* Promic Mount Plate: Extra slow (inner wall: 20, outer wall: 10)

## Print Time and Material Consumption for Individual Parts
All parts were printed with print settings as mentioned above and a line width of 0.4 mm.

### Model “Beta”

| Part              | Print Time | 
|-------------------|------------|
| Bottom Plate      | ~ 7h 30m   | 
| Middle (Walls)    | ~ 6h 50m   | 
| Finger Plate      | ~ 4h 20m   | 
| Thumb Plate       | ~ 2h 10m   | 
| Finger Back Plate |    ~ 55m   | 
| Thumb Back Plate  |    ~ 22m   | 
| Promic Mount Plate|    ~ 55m   | 
| Promic Back Bar   |    ~ 13m   | 
| Deco Base         | ~ 1h 45m   | 
| Deco Finger       | ~ 1h 35m   | 
| Deco Thumb        |    ~ 40m   | 

### Model “4rEnc”

| Part              | Print Time| Material |
|-------------------|-----------|----------|
| Bottom Plate      | ~ 7h 25m  | ~ 53g    |
| Middle (Walls)    | ~ 6h 21m  | ~ 42g    |
| Finger Plate      | ~ 4h 40m  | ~ 31g    |
| Thumb Plate       | ~ 2h 20m  | ~ 15g    |
| Finger Back Plate | ~ 1h 05m  | ~  8g    |
| Thumb Back Plate  |    ~ 21m  | ~  2g    |
| Promic Mount Plate|    ~ 53m  | ~  5g    |
| Promic Back Bar   |    ~ 13m  | ~  1g    |
| Deco Base         | ~ 1h 42m  | ~ 11g    |
| Deco Finger       | ~ 1h 27m  | ~  8g    |
| Deco Thumb        |    ~ 40m  | ~  3g    |

## Caps

