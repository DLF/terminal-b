# Parts and Material

## Screws

Terminal-B uses 3mm metric flat hat screws in different lengths.
Screws are normed by ISO 2009(DIN 963), the nuts by ISO 4032 (DIN 934).

About 20 nuts and screws are needed. The lengths of the screws depend on the exact Terminal-B model
(the configuration). The used lengths are taken from this lengths which are usually available in any
electronics or hardware store:

```
{5mm , 6mm , 8mm , 10mm , 12mm , 14mm , 16mm , 18mm , 20mm , 22mm , 25mm , 30mm}
```

For one of the bottom screws, most probably the one for the promic back-bar, can be chosen 4 or 6 mm longer on used to
“pin out” the reset pin from the controller by connecting a cable between the rst-pin and the screw, fixed by an additional
nut. Doing so allows “hot-wiring” the jack and that screw from the outside later to allow a hardware reset.

### Screw Lengths for specific Terminal-B configrations

#### Prototype, ver. 0.1.5
Needed: 6mm, 8mm, 10mm, 16mm, 20mm, and 30mm.

Per side:
* 5 × 16mm for thumb cluster
* 5 × 6mm for walls and promic side
* 5 × 8mm for back plate
* 1 × 8mm for promic back bar, or a 12mm long one if you want to conviniently connect that screw to the GND pin
* finger plate: 1 × 10mm, 1 × 16mm, 1 × 20mm, 2 × 30mm

#### R4Enc, ver. 1.0
Needed: 6mm , 8mm , 12mm , 14mm , 18mm , 30mm

Per side:
* 5 × 14mm for thumb cluster
* 5 × 6mm for walls and promic side
* 5 × 8mm for back plate
* 1 × 8mm for promic back bar, or a 12mm long one if you want to conviniently connect that screw to the GND pin
* finger plate: 1 × 12mm, 2 × 18mm, 2 × 30mm


