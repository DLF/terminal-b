# Terminal B

Terminal B is a DIY, [QMK](https://qmk.fm/) and Pro Micro driven, 3D-printable split keyboard.

This repository manages the 3D design only. If you want to build a Terminal B board,
you may either create your own QMK based firmware, or use mine as a base.

Terminal B is highly configurable. This includes basic attributes like the number of key rows and columns,
geometry aspects like tilt angles and corner roundings, and detailed aspects like clearances and the
swith geometry. As (almost) all measures are placed in configuration objects, there are several
hundrets of configuration options.

Terminal B is desinged in Python with [solidpython](https://github.com/SolidCode/SolidPython) as an
[OpenSCAD](http://openscad.org/) wrapper.

The design contains also the keycaps.
If you want to use standard keycaps, the board must be configured for that.


# Configurations
There are different configurations maintained for Terminal B.
However, the only built and tested configuration at the moment is ”*r4Enc*”.
The configuration “Beta” is based on a prototype version which was fully build,
but the current model was never tested.

## 3r
The basic configuration with 6 × 3 + 3 finger switches and 6 thumb switches, a y-tilt of 9° and a x-tilt of 3°.
The switch area is 19 mm × 18 mm, so default key caps won’t fit.

This configuration was not printed, built and tested so far.

## 4r
Like *3r* but with 4 key rows and an even smaller switch area of 19 mm × 17.4 mm.

Also this configuration was never built.

## 4rEnc
Like *4r*, but with a rotary encoder on each side, above the two outer columns.

This model has been successfully build.

## Beta
A special configuration based on a prototype configuration (version 0.1.5).

It also uses 6 × 3 + 3 finger swithes but has a much higher tilt: 12° for the y-tilt and 4° for the x-tilt.
It uses an area of 19.05 mm × 19.05 mm for each switch, so standard caps should fit, at least for the 
finger section. The vertical offset of the key columns is less pronounced for the pinky-columns and the
radius of the thumb cluster is smaller (thumb keys more thight).

The outer borders are wider while the lower border are shorter compared to *3r*.

Beta was never built but its configuration is a recreation of the mentioned prototype version,
which was built successfully and is in use since some months.
